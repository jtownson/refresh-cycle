## Cache refresh cycle

The 'refresh cycle' is a library that supports caching of rendered velocity templates. 
Thus content panels on a web page can be served from cache, giving a very large performance
benefit.

This is quite a complex library and I am digging out more docs from emails and other sources
from the time when the library was written. For now, 'ESI for velocity templates' is a simplistic
but close approximation. 
 
Data in caches is maintained by a background daemon which guarantees entries are fresh, so user
requests never block on refresh events. This is implemented using a technique called memoization.

There is JMX support for controlling the daemon. 