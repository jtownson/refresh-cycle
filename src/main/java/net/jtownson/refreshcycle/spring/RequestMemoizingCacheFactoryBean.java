package net.jtownson.refreshcycle.spring;

import net.jtownson.refreshcycle.RequestMemoizingCache;
import net.jtownson.refreshcycle.keybuilders.KeyBuilder;
import net.sf.ehcache.Ehcache;
import org.apache.commons.lang.Validate;

import java.util.Collections;
import java.util.List;

/**
 * @see RefreshCycleCollaboratorFactoryBean
 */
public class RequestMemoizingCacheFactoryBean extends SwitchingEhcacheFactoryBean {
    
    private List<KeyBuilder> keyBuilders;

    public RequestMemoizingCacheFactoryBean() {
        keyBuilders = Collections.EMPTY_LIST;
    }

    public RequestMemoizingCacheFactoryBean(List<KeyBuilder> keyBuilders) {
        setKeyBuilders(keyBuilders);
    }

    public List<KeyBuilder> getKeyBuilders() {
        return keyBuilders;
    }
    
    public void setKeyBuilders(List<KeyBuilder> keyBuilders) {
        Validate.notNull(keyBuilders);
        this.keyBuilders = keyBuilders;
    }

    @Override
    protected Ehcache decorateStep(Ehcache cache) {
        return new RequestMemoizingCache(cache, keyBuilders);
    }
}
