package net.jtownson.refreshcycle.spring;

import net.jtownson.refreshcycle.RefreshCycleSwitcher;
import net.sf.ehcache.Ehcache;
import org.springframework.cache.ehcache.EhCacheFactoryBean;

/**
 * Abstract base for factory classes that create cache decorators.
 * The switcher allows decoration to be turned on/off.
 */
public abstract class SwitchingEhcacheFactoryBean extends EhCacheFactoryBean {

    private RefreshCycleSwitcher switcher;

    public RefreshCycleSwitcher getSwitcher() {
        return switcher;
    }

    public void setSwitcher(RefreshCycleSwitcher switcher) {
        this.switcher = switcher;
    }

    protected abstract Ehcache decorateStep(Ehcache cache);

    @Override
    protected final Ehcache decorateCache(Ehcache cache) {
        if (switcher != null && switcher.isRefreshCycleSwitchedOn()) {
            return decorateStep(cache);
        }
        return cache;
    }
}
