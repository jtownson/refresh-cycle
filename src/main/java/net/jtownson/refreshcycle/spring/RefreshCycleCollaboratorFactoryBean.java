package net.jtownson.refreshcycle.spring;

import net.jtownson.refreshcycle.RefreshCycleCollaboratorImpl;
import net.sf.ehcache.Ehcache;

/**
 * Allows one to use spring to create RefreshCycleCollaboratorImpl
 * around a cache defined in ehcache.xml. Sample usage:
 * <pre>
 * <bean id="underlyingCache"
 * class="RefreshCycleCollaboratorFactoryBean">
 * <property name="cacheManager" ref="cacheManager"/>
 * <property name="cacheName" value="net.jtownson.opml.CACHE"/>
 * </bean>
 * </pre>
 */
public class RefreshCycleCollaboratorFactoryBean extends SwitchingEhcacheFactoryBean {

    @Override
    protected Ehcache decorateStep(Ehcache cache) {
        return new RefreshCycleCollaboratorImpl(cache);
    }
}
