package net.jtownson.refreshcycle;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.Status;
import net.sf.ehcache.constructs.EhcacheDecoratorAdapter;
import net.sf.ehcache.loader.CacheLoader;
import net.sf.ehcache.statistics.sampled.SampledCacheStatistics;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;

/**
 * This class is designed as a wrapper for hibernate caches.
 * If isInRefreshCycle is set to true then requests with
 * the x-jmt-refresh-caches http header will cause cache gets
 * to return null (thus presumably forcing calling code to
 * recalculate a value for that key and do a put).
 * Key->value pairs that *are* put while isInRefreshCycle is
 * true are considered *fresh*. Subsequent gets return
 * that value (not null) *even if* isInRefreshCycle is true.
 */
public class RefreshCycleCollaboratorImpl extends EhcacheDecoratorAdapter implements RefreshCycleCollaborator {

    private static final Logger LOGGER = Logger.getLogger(RefreshCycleCollaboratorImpl.class);

    private Set<Object> keysPutDuringRefreshCycle = Collections.synchronizedSet(new HashSet<Object>());
    private boolean isInRefreshCycle;

    public RefreshCycleCollaboratorImpl(Ehcache underlyingCache) {
        super(underlyingCache);
    }

    public void put(Element element) throws IllegalArgumentException, IllegalStateException, CacheException {
        if (isRefreshing()) {
            keysPutDuringRefreshCycle.add(element.getKey());
        }
        super.put(element);
    }

    public void put(Element element, boolean doNotNotifyCacheReplicators) throws IllegalArgumentException, IllegalStateException, CacheException {
        if (isRefreshing()) {
            keysPutDuringRefreshCycle.add(element.getKey());
        }
        super.put(element, doNotNotifyCacheReplicators);
    }

    public void putQuiet(Element element) throws IllegalArgumentException, IllegalStateException, CacheException {
        if (isRefreshing()) {
            keysPutDuringRefreshCycle.add(element.getKey());
        }
        super.putQuiet(element);
    }

    public void putWithWriter(Element element) throws IllegalArgumentException, IllegalStateException, CacheException {
        if (isRefreshing()) {
            keysPutDuringRefreshCycle.add(element.getKey());
        }
        super.putWithWriter(element);
    }

    public Element putIfAbsent(Element element) throws NullPointerException {
        if (isRefreshing()) {
            keysPutDuringRefreshCycle.add(element.getKey());
        }
        return super.putIfAbsent(element);
    }

    public Element get(Serializable key) throws IllegalStateException, CacheException {
        if (isKeyRefreshCandidate(key)) {
            return null;
        }
        return super.get(key);
    }

    public Element get(Object key) throws IllegalStateException, CacheException {
        if (isKeyRefreshCandidate(key)) {
            return null;
        }
        return super.get(key);
    }

    public Element getQuiet(Serializable key) throws IllegalStateException, CacheException {
        if (isKeyRefreshCandidate(key)) {
            return null;
        }

        return super.getQuiet(key);
    }

    public Element getQuiet(Object key) throws IllegalStateException, CacheException {
        if (isKeyRefreshCandidate(key)) {
            return null;
        }
        return super.getQuiet(key);
    }

    public List getKeys() throws IllegalStateException, CacheException {
        if (isRefreshCachesHeaderSet()) {
            return null;
        }
        return super.getKeys();
    }

    public List getKeysWithExpiryCheck() throws IllegalStateException, CacheException {
        if (isRefreshCachesHeaderSet()) {
            return null;
        }
        return super.getKeysWithExpiryCheck();
    }

    public List getKeysNoDuplicateCheck() throws IllegalStateException {
        if (isRefreshCachesHeaderSet()) {
            return null;
        }
        return super.getKeysNoDuplicateCheck();
    }

    public int getSize() throws IllegalStateException, CacheException {
        if (isRefreshCachesHeaderSet()) {
            return 0;
        }
        return super.getSize();
    }

    public int getSizeBasedOnAccuracy(int statisticsAccuracy) throws IllegalArgumentException, IllegalStateException, CacheException {
        if (isRefreshCachesHeaderSet()) {
            return 0;
        }
        return super.getSizeBasedOnAccuracy(statisticsAccuracy);
    }

    public long getMemoryStoreSize() throws IllegalStateException {
        if (isRefreshCachesHeaderSet()) {
            return 0L;
        }
        return super.getMemoryStoreSize();
    }

    public int getDiskStoreSize() throws IllegalStateException {
        if (isRefreshCachesHeaderSet()) {
            return 0;
        }
        return super.getDiskStoreSize();
    }

    public Status getStatus() {
        if (isRefreshCachesHeaderSet()) {
            return null;
        }
        return super.getStatus();
    }

    public String getGuid() {
        if (isRefreshCachesHeaderSet()) {
            return null;
        }
        return super.getGuid();
    }

    public Element getWithLoader(Object key, CacheLoader loader, Object loaderArgument) throws CacheException {
        if (isRefreshCachesHeaderSet()) {
            return null;
        }
        return super.getWithLoader(key, loader, loaderArgument);
    }

    public Map getAllWithLoader(Collection keys, Object loaderArgument) throws CacheException {
        if (isRefreshCachesHeaderSet()) {
            return null;
        }
        return super.getAllWithLoader(keys, loaderArgument);
    }

    public SampledCacheStatistics getSampledCacheStatistics() {
        if (isRefreshCachesHeaderSet()) {
            return null;
        }
        return super.getSampledCacheStatistics();
    }

    public Object getInternalContext() {
        if (isRefreshCachesHeaderSet()) {
            return null;
        }
        return super.getInternalContext();
    }

    @SuppressWarnings({"EqualsWhichDoesntCheckParameterClass"})
    @Override
    public boolean equals(Object obj) {
        return underlyingCache.equals(obj);
    }

    public synchronized void setIsInRefreshCycle(boolean b, Set<HttpRequestAdapter> requestSubset) {
        if ( ! isInRefreshCycle && b) { // cycle starting
            keysPutDuringRefreshCycle.clear();
        } else if (isInRefreshCycle && ! b) { // cycle ending
            logUpdatedKeys();
            keysPutDuringRefreshCycle.clear();
        }

        isInRefreshCycle = b;
    }

    // force refresh (via the header x-jmt-force-synchronize-caches)
    // makes the caches sync, even outside of a refresh cycle.
    // that allows the user to refresh a single url.
    private boolean isForceRefreshHeaderSet() {
        HttpServletRequest request = getRequest();
        return request != null && getRequest().getHeader(RefreshConstants.FORCE_SYNCHRONIZE_COLLABORATING_CACHES_HEADER) != null;
    }

    private boolean isRefreshing() {
        return (isInRefreshCycle && isRefreshCachesHeaderSet()) || isForceRefreshHeaderSet();
    }
    
    private void logUpdatedKeys() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(MessageFormat.format("Updated {0} keys for cache {1}: {2}.",
                    keysPutDuringRefreshCycle.size(), getName(), StringUtils.join(keysPutDuringRefreshCycle, ',')));
        }
    }

    private boolean isRefreshCachesHeaderSet() {
        boolean isACandidate = false;
        Object requestObject = MDC.get(RefreshCycleFilter.HTTP_REQUEST);
        if (requestObject instanceof HttpServletRequest) {
            HttpServletRequest request = (HttpServletRequest)requestObject;
            isACandidate = Boolean.parseBoolean(request.getHeader(RefreshConstants.SYNCHRONIZE_COLLABORATING_CACHES_HEADER));
        }
        return isACandidate;
    }

    private HttpServletRequest getRequest() {
        Object requestObject = MDC.get(RefreshCycleFilter.HTTP_REQUEST);
        if (requestObject instanceof HttpServletRequest) {
            return (HttpServletRequest)requestObject;
        }
        return null;        
    } 
    private boolean isKeyRefreshCandidate(Object key) {
        return isRefreshing() && ! isKeyAlreadyRefreshed(key);
    }

    private boolean isKeyAlreadyRefreshed(Object key) {
        return keysPutDuringRefreshCycle.contains(key);
    }

}
