package net.jtownson.refreshcycle;

import net.jtownson.refreshcycle.keybuilders.KeyBuilder;
import net.jtownson.refreshcycle.keybuilders.UrlKeyBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.WordUtils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;

/**
 *
 */
public class HttpRequestAdapter implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String EMPTY_STRING = "";
    private static final String NEWLINE = "\r\n";
    private static final char[] HEADER_CAPITALIZATION_DELIMITER = {'-'};
    private String scheme = "HTTP";
    private String method = "GET";
    private String uri;
    private Map<String, String> headers;
    private String body;
    private int requestHash;
    private String requestKey;

    public HttpRequestAdapter() {
        headers = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
    }

    public HttpRequestAdapter(HttpServletRequest request, List<KeyBuilder> keyBuilders, Set<String> refreshRequestHeaders) {
        method = request.getMethod();
        uri = UrlKeyBuilder.getUrl(request);
        scheme = request.getScheme();
        headers = getHeaders(request, refreshRequestHeaders);
        body = getRequestBody(request);
        doSetRequestKey(request, keyBuilders);
    }

    @Override
    public int hashCode() {
        return requestHash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof HttpRequestAdapter)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        HttpRequestAdapter lhs = this;
        HttpRequestAdapter rhs = (HttpRequestAdapter) obj;

        return lhs.requestHash == rhs.requestHash;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0}://{1}{2}", getScheme().toLowerCase(), getHeaders().get("host"), getUri());
    }

    public String getMethod() {
        return method;
    }

    public String getUri() {
        return uri;
    }

    public String getScheme() {
        return scheme;
    }

    public Map<String, String> getHeaders() {
        return Collections.unmodifiableMap(headers);
    }

    public void addHeader(String name, String value) {
        headers.put(normalizeHeaderCase(name), value);
    }

    public String getBody() {
        return body;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRequestKey() {
        return requestKey;
    }

    public void setRequestKey(String requestKey) {
        this.requestKey = requestKey;
        requestHash = requestKey.hashCode();
    }

    // turn user-agent into User-Agent, etc
    public static String normalizeHeaderCase(String header) {
        return WordUtils.capitalize(header, HEADER_CAPITALIZATION_DELIMITER);
    }

    /*
    private Map<String, String> getHeaders(HttpServletRequest request, Set<String> allowableHeaders) {

        String headers = FilterUtils.getHeaders(request, NEWLINE, allowableHeaders);
        String [] headerArr = headers.split(NEWLINE);
        Map<String, String> headerMap = new LinkedHashMap<String, String>(headerArr.length);
        for (String header : headerArr) {
            String [] keyValPair = header.split("\\s*:\\s*");
            if (keyValPair.length > 0 && StringUtils.isNotBlank(keyValPair[0])) {
                if (keyValPair.length > 1) {
                    headerMap.put(keyValPair[0], keyValPair[1]);
                } else {
                    headerMap.put(keyValPair[0], "");
                }
            }
        }
        return headerMap;
    }*/

    private Map<String, String> getHeaders(HttpServletRequest request, Set<String> allowableHeaders) {
        Map<String, String> headerMap = new LinkedHashMap<String, String>();
        for(String header : allowableHeaders) {
            headerMap.put(header, defaultIfNull(request.getHeader(header), ""));
        }
        return headerMap;
    }

    private static <T> T defaultIfNull(T value, T def) {
        return value == null ? def : value;
    }

    private String getRequestBody(HttpServletRequest request) {
        try {
            ServletInputStream bodyStream = request.getInputStream();
            try {
                if (bodyStream != null) {
                    return IOUtils.toString(bodyStream);
                }
            } finally {
                if (bodyStream != null) {bodyStream.close();}
            }
        } catch(IOException ex) {

        }
        return EMPTY_STRING;
    }

    private void doSetRequestKey(HttpServletRequest request, List<KeyBuilder> keyBuilders) {
        StringBuilder stringBuilder = new StringBuilder();
        for (KeyBuilder keyBuilder : keyBuilders) {
            keyBuilder.appendToKey(request, stringBuilder);
        }
        setRequestKey(stringBuilder.toString());
    }
}
