package net.jtownson.refreshcycle;

import net.jtownson.refreshcycle.keybuilders.CompositeKeyBuilder;
import net.jtownson.refreshcycle.keybuilders.KeyBuilder;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.constructs.blocking.BlockingCache;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.javatuples.Pair;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;

/**
 * Memoize the request context of cache puts, to compile a
 * request table that can be replayed in order to
 * refresh the cache.
 *
 * The refresh mechanism is as follows:
 *
 * When keys are put into the cache (by the fragment tool), the http request info is saved here.
 *
 * The RefreshCycleManager mbean gets all the memoized http requests and calls them (on the same machine).
 *
 * A special x-jmt-refresh-cache-region header is set for every refresh request.
 * If it includes the name of this cache, all gets return null. This 'tricks' the
 * fragment tool into regenerating the data and putting a fresh value.
 *
 * The entire cycle of requests should be demarked by setIsInRefreshCycle calls
 * to that the cache can see whether any keys have been missed by the refresh
 * requests. Missed keys are expired.
 */
public class RequestMemoizingCache extends BlockingCache implements RefreshCycleCollaborator {

    public static final String REQUEST_KEY_BUILDER_OVERRIDE_ATTR
            = "com.biomedcentral.journal.cache.refreshablecache.keybuilders";

    private final static Logger LOGGER = Logger.getLogger(RequestMemoizingCache.class);
    private final List<KeyBuilder> defaultKeyBuilders;
    private final Set<Object> refreshableKeySet = Collections.synchronizedSet(new HashSet<Object>());

    private boolean isInRefreshCycle;
    
    private Ehcache myUnderlyingCache;

    public RequestMemoizingCache(Ehcache underlyingCache, List<KeyBuilder> defaultKeyBuilders) {

        super(underlyingCache);

        myUnderlyingCache = underlyingCache;

        Validate.notNull(defaultKeyBuilders);

        this.defaultKeyBuilders = defaultKeyBuilders;

        makeBackingCacheEternal(underlyingCache);
    }

    @Override
    public Element get(Object key) throws IllegalStateException, CacheException {
        if (isInContextOfRefreshRequest()) {
            return null;
        }
        Pair<Element, HttpRequestAdapter> e = getFromUnderlyingCache(key);
        return e == null ? null : e.getValue0();
    }

    @Override
    public Element get(Serializable key) throws IllegalStateException, CacheException {
        if (isInContextOfRefreshRequest()) {
            return null;
        }
        Pair<Element, HttpRequestAdapter> e = getFromUnderlyingCache(key);
        return e == null ? null : e.getValue0();
    }

    @Override
    public void put(Element element, boolean doNotNotifyCacheReplicators) throws IllegalArgumentException, IllegalStateException, CacheException {
        if (isInContextOfRefreshRequest()) {
            markKeyAsRefreshed(element.getKey());
        }
        super.put(getMemoizedElement(element), doNotNotifyCacheReplicators);
    }

    @Override
    public void put(Element element) throws IllegalArgumentException, IllegalStateException, CacheException {
        if (isInContextOfRefreshRequest()) {
            markKeyAsRefreshed(element.getKey());
        }
        super.put(getMemoizedElement(element));
    }

    @Override
    public void putQuiet(Element element) throws IllegalArgumentException, IllegalStateException, CacheException {
        if (isInContextOfRefreshRequest()) {
            markKeyAsRefreshed(element.getKey());
        }
        super.putQuiet(getMemoizedElement(element));
    }

    @Override
    public void putWithWriter(Element element) throws IllegalArgumentException, IllegalStateException, CacheException {
        if (isInContextOfRefreshRequest()) {
            markKeyAsRefreshed(element.getKey());
        }
        super.putWithWriter(getMemoizedElement(element));
    }

    @SuppressWarnings("unchecked")
    public Set<HttpRequestAdapter> getMemoizedRequestSet() {

        List<String> keys = getKeysNoDuplicateCheck();

        Set<HttpRequestAdapter> memoizedRequests = new LinkedHashSet<HttpRequestAdapter>(keys.size());

        for (String key : keys) {
            Pair<Element, HttpRequestAdapter> elementPair = getFromUnderlyingCache(key);
            if (elementPair != null) { // sometimes keys returned by getKeysNoDuplicateCheck are expired, so null
                HttpRequestAdapter requestAdapter = elementPair.getValue1();
                if (requestAdapter != null) {
                    memoizedRequests.add(requestAdapter);
                } else {
                    LOGGER.warn(MessageFormat.format("Memoization error. Missing request adapter for cache key {0}. That is odd.", key));
                }
            }
        }

        return memoizedRequests;
    }

    @SuppressWarnings("unchecked")
    public void setIsInRefreshCycle(boolean b) {

        setIsInRefreshCycle(b, null);
    }

    @SuppressWarnings("unchecked")
    public void setIsInRefreshCycle(boolean b, Set<HttpRequestAdapter> requestSubset) {

        if ( ! isInRefreshCycle && b) {
        // cycle beginning
        // 1) foreach cache element
        // 2) if request adapter is in our request subset
        // 3) mark it as a refresh candidate
        // 4) so that when the cycle completes we can see which keys have not refreshed correctly
        // 5) (and evict them).

            refreshableKeySet.clear();
            List<String> keys = getKeysNoDuplicateCheck();
            if (requestSubset == null) {
                refreshableKeySet.addAll(keys);
            } else {
                for (String key : keys) {
                    Pair<Element, HttpRequestAdapter> elementPair = getFromUnderlyingCache(key);
                    if (elementPair != null) { // sometimes keys returned by getKeysNoDuplicateCheck are expired, so null
                        Element ehcacheElement = elementPair.getValue0();
                        String requestKey = (String)ehcacheElement.getKey();
                        long hitCount = ehcacheElement.getHitCount();
                        HttpRequestAdapter requestAdapter = elementPair.getValue1();
                        if (requestSubset.contains(requestAdapter)) {
                            refreshableKeySet.add(requestKey);
                        }
                    }
                }
            }
        }

        else if (isInRefreshCycle && ! b) { // cycle completing
            removeNonRefreshedKeys();
            refreshableKeySet.clear();
        }

        else {
            throw new IllegalStateException("Somebody made a duplicate call to setIsInRefreshCycle.");
        }

        isInRefreshCycle = b;
    }

    @SuppressWarnings({"EqualsWhichDoesntCheckParameterClass"})
    @Override
    public boolean equals(Object obj) {
        return myUnderlyingCache.equals(obj);
    }

    private void removeNonRefreshedKeys() {
        for (Object key : refreshableKeySet) {
            LOGGER.debug(MessageFormat.format("Cache key {0} was not refreshed during cycle. Removing cache entry.", key));
            remove(key);
        }
    }

    private void markKeyAsRefreshed(Object key) {

        boolean removed = refreshableKeySet.remove(key);
        if (removed) {
            LOGGER.debug(MessageFormat.format("Refreshing cache key {0}.", key));
        } else {
            LOGGER.debug(MessageFormat.format("Cache key {0} not removed during refresh. Is this an error?", key));
        }
    }

    private void makeBackingCacheEternal(Ehcache underlyingCache) {
        CacheConfiguration cacheConfiguration = underlyingCache.getCacheConfiguration();
        if (cacheConfiguration != null) {
            cacheConfiguration.eternal(true);
        }
    }

    private Pair<Element, HttpRequestAdapter> getFromUnderlyingCache(Serializable key) {
        Element e = underlyingCache.get(key);        
        return e == null ? null : (Pair<Element, HttpRequestAdapter>)e.getValue();
    }

    private Pair<Element, HttpRequestAdapter> getFromUnderlyingCache(Object key) {
        Element e = underlyingCache.get(key);
        return e == null ? null : (Pair<Element, HttpRequestAdapter>)e.getValue();
    }

    private Element getMemoizedElement(Element clientsElement) {
        return new Element(clientsElement.getKey(), memoizeRequest(clientsElement));
    }

    private Pair<Element, HttpRequestAdapter> memoizeRequest(Element element) {
        HttpServletRequest request = getRequest();
        if (request != null) {
            HttpRequestAdapter requestAdapter = getRequestAdapter(request);
            LOGGER.debug(MessageFormat.format("Memoizing request {0} for cache key {1}.", requestAdapter, element.getKey()));
            return new Pair<Element, HttpRequestAdapter>(element, requestAdapter);
        }
        LOGGER.debug(MessageFormat.format("Memoization error. No request context for cache key {0}.", element.getKey()));
        return new Pair<Element, HttpRequestAdapter>(element, null);
    }

    private HttpRequestAdapter getRequestAdapter(HttpServletRequest request) {

        List<KeyBuilder> keyBuilders = (List<KeyBuilder>)request.getAttribute(REQUEST_KEY_BUILDER_OVERRIDE_ATTR);

        if (keyBuilders == null) {
            keyBuilders = defaultKeyBuilders;
        }

        return CompositeKeyBuilder.getRequestAdapter(request, keyBuilders);
    }

    private Boolean isInContextOfRefreshRequest() {

        HttpServletRequest request = getRequest();

        if (request != null) {

            boolean force = request.getHeader(RefreshConstants.FORCE_SYNCHRONIZE_COLLABORATING_CACHES_HEADER) != null;

            if (force) {
                return true;
            }

            Enumeration refreshRegionsEnum = request.getHeaders(RefreshConstants.REFRESH_CACHE_REGION_HEADER);
            if (refreshRegionsEnum != null) {
                while (refreshRegionsEnum.hasMoreElements()) {
                    String headerVal = (String)refreshRegionsEnum.nextElement();
                    if (headerVal != null && headerVal.contains(getName())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private HttpServletRequest getRequest() {
        Object requestObject = MDC.get(RefreshCycleFilter.HTTP_REQUEST);
        if (requestObject != null && requestObject instanceof HttpServletRequest) {
            return (HttpServletRequest)requestObject;
        }
        return null;
    }
}
