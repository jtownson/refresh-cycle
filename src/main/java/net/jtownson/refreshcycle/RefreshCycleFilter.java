package net.jtownson.refreshcycle;

import org.apache.log4j.MDC;

import javax.servlet.*;
import java.io.IOException;

/**
 *
 */
public class RefreshCycleFilter implements Filter {

    public static final String HTTP_REQUEST = "http-request";

    public void init(FilterConfig filterConfig) throws ServletException { }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        MDC.put(HTTP_REQUEST, servletRequest);

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() { }
}
