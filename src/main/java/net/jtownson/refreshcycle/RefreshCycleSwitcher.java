package net.jtownson.refreshcycle;

import org.apache.commons.lang.Validate;

/**
 *
 */
public class RefreshCycleSwitcher {

    private final String switcherPropertyName;

    public RefreshCycleSwitcher(String switcherPropertyName) {
        Validate.notEmpty(switcherPropertyName);

        this.switcherPropertyName = switcherPropertyName;
    }

    public boolean isRefreshCycleSwitchedOn() {
        return Boolean.getBoolean(switcherPropertyName);
    }
}
