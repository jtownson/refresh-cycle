package net.jtownson.refreshcycle.keybuilders;

import net.jtownson.refreshcycle.HttpRequestAdapter;

import javax.servlet.http.HttpServletRequest;

/**
 *  Modifies cache key based on http request method.
 */
public class RequestMethodKeyBuilder implements KeyBuilder {
    public void appendToKey(HttpServletRequest request, StringBuilder stringBuilder) {
        stringBuilder.append(request.getMethod().toUpperCase());
    }

    public void appendToRequest(HttpServletRequest request, HttpRequestAdapter requestAdapter) {
        String method = request.getMethod();
        requestAdapter.setMethod(method);
    }
}
