package net.jtownson.refreshcycle.keybuilders;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import javax.servlet.http.HttpServletRequest;

/**
 *
 */
public interface KeyBuilder {

    void appendToKey(HttpServletRequest request, StringBuilder stringBuilder);
    void appendToRequest(HttpServletRequest request, HttpRequestAdapter requestAdapter);
}
