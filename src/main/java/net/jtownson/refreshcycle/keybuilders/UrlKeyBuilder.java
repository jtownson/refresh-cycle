package net.jtownson.refreshcycle.keybuilders;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 */
public class UrlKeyBuilder implements KeyBuilder {

    public void appendToKey(HttpServletRequest request, StringBuilder stringBuilder) {
        // sort the url params and add to hashcode
        TreeMap<String, String[]> sortedParameterMap = new TreeMap(request.getParameterMap());
        for (Map.Entry<String, String[]> keyVal : sortedParameterMap.entrySet()) {
            stringBuilder.append(keyVal.getKey());
            stringBuilder.append(StringUtils.join(sort(keyVal.getValue()), ""));
        }

        // append the uri
        String uri = request.getRequestURI();
        if (uri != null) {
            stringBuilder.append(uri);
        }
    }

    public void appendToRequest(HttpServletRequest request, HttpRequestAdapter requestAdapter) {
        requestAdapter.setUri(getUrl(request));
    }

    public static String getUrl(HttpServletRequest request) {
        String uri = request.getRequestURI();
        String queryString = request.getQueryString();

        if (queryString == null) {
            return uri;
        } else {
            if (queryString.startsWith("&")) {
                queryString = queryString.substring(1, queryString.length());
            }
            return uri + '?' + queryString;
        }
    }

    private String [] sort(String [] s) {
        Arrays.sort(s);
        return s;
    }
}
