package net.jtownson.refreshcycle.keybuilders;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.apache.commons.lang.Validate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class CookieKeyBuilder implements KeyBuilder {

    private final String domain;
    private final String path;
    private final String name;
    private final List<Pattern> valueExpressions;

    // these c'tors take a single regex to match the cookie value
    public CookieKeyBuilder(String name, String valueExpression) {
        this(null, null, name, Arrays.asList(valueExpression));
    }

    public CookieKeyBuilder(String path, String name, String valueExpression) {
        this(null, path, name, Arrays.asList(valueExpression));
    }

    public CookieKeyBuilder(String domain, String path, String name, String valueExpression) {
        this(domain, path, name, Arrays.asList(valueExpression));
    }

    // these c'tors take a list of regex (e.g. to match both usrid=\d+ and institution_id=\d+)
    public CookieKeyBuilder(String name, List<String> valueExpressions) {
        this(null, null, name, valueExpressions);
    }

    public CookieKeyBuilder(String path, String name, List<String> valueExpressions) {
        this(null, path, name, valueExpressions);
    }

    public CookieKeyBuilder(String domain, String path, String name, List<String> valueExpressions) {
        Validate.notNull(name);
        Validate.notNull(valueExpressions);

        this.domain = domain;
        this.path = path;
        this.name = name;
        this.valueExpressions = compileValueExpressions(valueExpressions);
    }

    public void appendToRequest(HttpServletRequest request, HttpRequestAdapter requestAdapter) {

        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public void appendToKey(HttpServletRequest request, StringBuilder stringBuilder) {

        Cookie[] cookies = request.getCookies();
        if (cookies == null || cookies.length == 0) {
            return;
        }

        String[] relevantCookieFields = new String[4];

        for (Cookie cookie : cookies) {
            appendToKey(stringBuilder, relevantCookieFields, cookie);
        }
    }

    private void appendToKey(StringBuilder stringBuilder, String[] relevantCookieFields, Cookie cookie) {

        // cookie name criterion is compulsory.
        if (!name.equals(cookie.getName())) {
            return;
        } else {
            relevantCookieFields[0] = cookie.getName();
        }

        // if domain criterion is set then check it
        if (domain != null && !domain.equalsIgnoreCase(cookie.getDomain())) {
            return;
        } else if (domain == null) {
            // there's no domain criterion just make sure the coookies domain it doesn't
            // affect the hashcode.
            relevantCookieFields[1] = null;
        } else {
            // there was a domain criterion set and it matches the cookies domain.
            // it contributes to the hash.
            relevantCookieFields[1] = cookie.getDomain();
        }

        // same pattern for path criterion
        if (path != null && !path.equals(cookie.getPath())) {
            return;
        } else if (path == null) {
            relevantCookieFields[2] = null;
        } else {
            relevantCookieFields[2] = cookie.getPath();
        }

        List<String> cookieValueMatches = valueRegexMatches(cookie);
        if (cookieValueMatches == null) {
            // then the other fields matched
            // but the value didn't
            return;
        } else {

            // this cookie matches all fields and its value matches
            // build a hashcode from it.
            for (String relevantCookieField : relevantCookieFields) {
                if (relevantCookieField != null) {
                    stringBuilder.append(relevantCookieField);
                }
            }
            for (String cookieValueMatch : cookieValueMatches) {
                if (cookieValueMatch != null) {
                    stringBuilder.append(cookieValueMatch);
                }
            }
        }
    }

    private List<String> valueRegexMatches(Cookie cookie) {
        ArrayList<String> cookieValueMatches = new ArrayList<String>();
        for (Pattern valueExpression : valueExpressions) {

            Matcher m = valueExpression.matcher(cookie.getValue());
            boolean found = m.find();

            if (found) {
                int groupCount = m.groupCount();
                if (groupCount > 0) {
                    String[] valueMatches = new String[m.groupCount()];
                    for (int i = 0; i < valueMatches.length; i++) {
                        valueMatches[i] = m.group(i);
                    }
                    cookieValueMatches.addAll(Arrays.asList(valueMatches));
                } else {
                    cookieValueMatches.add(m.group());
                }
            }
        }
        return cookieValueMatches;
    }

    private List<Pattern> compileValueExpressions(List<String> valueExpressions) {
        ArrayList<Pattern> patterns = new ArrayList<Pattern>(valueExpressions.size());
        for(String expression : valueExpressions) {
            patterns.add(Pattern.compile(expression));
        }
        return patterns;
    }
}
