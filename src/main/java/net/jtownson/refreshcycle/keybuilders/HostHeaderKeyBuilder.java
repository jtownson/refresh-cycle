package net.jtownson.refreshcycle.keybuilders;

/**
 *
 */
public class HostHeaderKeyBuilder extends HeaderKeyBuilder {

    public HostHeaderKeyBuilder() {
        super("Host");
    }

}
