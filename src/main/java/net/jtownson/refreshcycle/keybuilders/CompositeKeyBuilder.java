package net.jtownson.refreshcycle.keybuilders;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.apache.commons.lang.Validate;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Helper to save duplicated code that loops over KeyBuilder lists.
 */
public class CompositeKeyBuilder implements KeyBuilder {

    private List<KeyBuilder> keyBuilders;

    private CompositeKeyBuilder(List<KeyBuilder> keyBuilders) {
        Validate.notNull(keyBuilders);
        this.keyBuilders = keyBuilders;
    }

    public static String getRequestKey(HttpServletRequest request, List<KeyBuilder> keyBuilders) {
        CompositeKeyBuilder keyBuilder = new CompositeKeyBuilder(keyBuilders);
        return keyBuilder.buildRequestKey(request);
    }

    public static HttpRequestAdapter getRequestAdapter(HttpServletRequest request, List<KeyBuilder> keyBuilders) {
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        CompositeKeyBuilder keyBuilder = new CompositeKeyBuilder(keyBuilders);
        keyBuilder.appendToRequest(request, requestAdapter);
        requestAdapter.setRequestKey(keyBuilder.buildRequestKey(request));
        return requestAdapter;
    }

    public void appendToKey(HttpServletRequest request, StringBuilder stringBuilder) {
        stringBuilder.append(buildRequestKey(request));
    }

    public void appendToRequest(HttpServletRequest request, HttpRequestAdapter requestAdapter) {
        for(KeyBuilder keyBuilder : keyBuilders) {
            keyBuilder.appendToRequest(request, requestAdapter);
        }
    }

    private String buildRequestKey(HttpServletRequest request) {
        StringBuilder stringBuilder = new StringBuilder();
        for(KeyBuilder keyBuilder : keyBuilders) {
            keyBuilder.appendToKey(request, stringBuilder);
            stringBuilder.append(':');
        }
        if (stringBuilder.length() == 0) {
            return "";
        }
        return stringBuilder.substring(0, stringBuilder.lastIndexOf(":"));
    }

}
