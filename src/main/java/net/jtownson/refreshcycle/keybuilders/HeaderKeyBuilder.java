package net.jtownson.refreshcycle.keybuilders;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.apache.commons.lang.Validate;

import javax.servlet.http.HttpServletRequest;

/**
 *
 */
public class HeaderKeyBuilder implements KeyBuilder {

    private final String headerName;

    public HeaderKeyBuilder(String headerName) {
        Validate.notNull(headerName);
        this.headerName = headerName;
    }

    public void appendToKey(HttpServletRequest request, StringBuilder stringBuilder) {

        stringBuilder.append(headerName).append(": ");

        String header = request.getHeader(headerName);
        if (header != null) {
            stringBuilder.append(header.toLowerCase());
        }
    }

    public void appendToRequest(HttpServletRequest request, HttpRequestAdapter requestAdapter) {

        String header = request.getHeader(headerName);

        if (header == null) {
            header = "";
        }

        requestAdapter.addHeader(headerName, header);
    }
}
