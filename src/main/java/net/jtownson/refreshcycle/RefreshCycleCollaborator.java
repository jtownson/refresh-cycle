package net.jtownson.refreshcycle;

import java.util.Set;

/**
 *
 */
public interface RefreshCycleCollaborator {

    void setIsInRefreshCycle(boolean b, Set<HttpRequestAdapter> requestSubset);
}
