package net.jtownson.refreshcycle.management;

/**
 *
 */
public interface RefreshCycleManagerMBean {

    void refresh();

    String showRefreshRequests(String hostPattern, String uriPattern, String querySubstring);

    void refresh(String hostPattern, String uriPattern, String querySubstring);

    void cancelTimer();

    void reinstallTimer();

    long getRefreshPeriodSecs();

    void setRefreshPeriodSecs(long refreshPeriodSecs);

    void clearAllExceptRefreshCycleCaches();

    String connectionSelfTest();

    boolean isRunning();

    boolean isEnabled();
}
