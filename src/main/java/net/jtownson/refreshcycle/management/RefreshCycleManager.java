package net.jtownson.refreshcycle.management;

import net.jtownson.refreshcycle.*;
import net.jtownson.refreshcycle.requestfilters.*;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.util.*;

/**
 * Management of the cache refresh cycle.
 */
public class RefreshCycleManager implements RefreshCycleManagerMBean {

    private static final Logger LOGGER = Logger.getLogger(RefreshCycleManager.class);

    private final CacheManager cacheManager;
    private final RequestMaker requestMaker;
    private final RefreshCycleSwitcher switcher;
    private Timer timer;
    private long refreshPeriodSecs;
    private boolean isRunning;

    public RefreshCycleManager(CacheManager cacheManager, RequestMaker requestMaker, RefreshCycleSwitcher switcher,
                               long refreshPeriodSecs) {

        Validate.notNull(cacheManager);
        Validate.notNull(requestMaker);
        Validate.notNull(switcher);

        this.cacheManager = cacheManager;
        this.requestMaker = requestMaker;
        this.switcher = switcher;
        this.refreshPeriodSecs = refreshPeriodSecs;

        scheduleRefreshTask();
    }

    public boolean isEnabled() {
        return switcher.isRefreshCycleSwitchedOn();
    }

    public void refresh() {
        backGroundRefresh(new IdentityRequestFilter(), cacheManager.getCacheNames());
    }

    public void refresh(String cacheName) {
        backGroundRefresh(new IdentityRequestFilter(), cacheName);
    }

    public boolean isRefreshable(String cacheName) {
        return cacheManager.getEhcache(cacheName) instanceof RequestMemoizingCache;
    }

    public void refresh(String hostPattern, String uriPattern, String querySubstring) {

        HttpRequestFilter[] requestFilterArray = getFiltersForStringPatterns(hostPattern, uriPattern, querySubstring);

        CompositeRequestFilter requestFilter = new CompositeRequestFilter(requestFilterArray);

        backGroundRefresh(requestFilter, cacheManager.getCacheNames());
    }

    public String showRefreshRequests(String hostPattern, String uriPattern, String querySubstring) {

        HttpRequestFilter[] requestFilterArray = getFiltersForStringPatterns(hostPattern, uriPattern, querySubstring);

        CompositeRequestFilter requestFilter = new CompositeRequestFilter(requestFilterArray);

        List<RequestMemoizingCache> memoizingCaches = getMemoizingCaches(cacheManager.getCacheNames());

        Set<HttpRequestAdapter> requestSet = new LinkedHashSet<HttpRequestAdapter>();

        for (RequestMemoizingCache memoizingCache : memoizingCaches) {
            requestSet.addAll(memoizingCache.getMemoizedRequestSet());
        }
        requestSet = requestFilter.filter(requestSet);

        StringBuilder answerBuilder = new StringBuilder();
        for (HttpRequestAdapter adapter : requestSet) {
            answerBuilder.append(adapter.toString());
            answerBuilder.append('\n');
        }
        return answerBuilder.toString();
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void cancelTimer() {
        if (timer != null) {
            timer.cancel();
        }
        LOGGER.info("Cache refresh cycle cancelled.");
    }

    public void reinstallTimer() {
        cancelTimer();
        scheduleRefreshTask();
    }

    public long getRefreshPeriodSecs() {
        return refreshPeriodSecs;
    }

    public void setRefreshPeriodSecs(long refreshPeriodSecs) {
        this.refreshPeriodSecs = refreshPeriodSecs;
        reinstallTimer();
    }

    public void clearAllExceptRefreshCycleCaches() {
        String [] cacheNames = cacheManager.getCacheNames();
        for (String cacheName : cacheNames) {
            Ehcache cache = cacheManager.getEhcache(cacheName);
            if ( cache != null && ! (cache instanceof RequestMemoizingCache)) {
                cache.removeAll();
            }
        }
    }

    public String connectionSelfTest() {
        if (requestMaker.isConnectionAvailable()) {
            return "Connection okay.";
        }
        return "Connection failed.\n" +
                "Check server.http.listen.address and server.http.listen.port in server.properties\n" +
                "and/or tomcat http configuration.";
    }

    private HttpRequestFilter[] getFiltersForStringPatterns(String hostPattern, String uriPattern, String querySubstring) {
        ArrayList<HttpRequestFilter> requestFilters = new ArrayList<HttpRequestFilter>(3);
        if ( ! StringUtils.isEmpty(hostPattern)) {
            requestFilters.add(new HostNameRequestFilter(hostPattern));
        }
        if ( ! StringUtils.isEmpty(uriPattern)) {
            requestFilters.add(new UriRequestFilter(uriPattern));
        }
        if ( ! StringUtils.isEmpty(querySubstring)) {
            requestFilters.add(new QueryStringRequestFilter(querySubstring));
        }

        return requestFilters.toArray(new HttpRequestFilter[0]);
    }

    private void backGroundRefresh(final HttpRequestFilter requestFilter, final String ... cacheNames) {
        Thread t = new Thread(createRefreshTask(requestFilter, cacheNames));
        t.setDaemon(true);
        t.start();
    }

    private TimerTask createRefreshTask(final HttpRequestFilter requestFilter, final String... cacheNames) {
        // TODO extract this task class -- possibly create a task factory
        return new TimerTask() {
            public void run() {
                synchronized (this) {
                    if (isRunning) {
                        LOGGER.info("Cache refresh failed to start. Already running.");
                        return;
                    } else if ( ! isRefreshCycleSwitchedOn()) {
                        LOGGER.info("Cache refresh failed to start. Feature is switched off ");
                        return;
                    }

                    isRunning = true;
                }
                try {
                    LOGGER.info("Cache refresh cycle starting.");
                    doRefresh(requestFilter, cacheNames);
                } finally {
                    isRunning = false;
                    LOGGER.info("Cache refresh cycle complete.");
                }
            }
        };
    }

    private void scheduleRefreshTask() {

        if (refreshPeriodSecs != 0 && isRefreshCycleSwitchedOn()) {

            timer = new Timer("cache-refresh-thread", true);

            long initialDelay = refreshPeriodSecs;

            timer.scheduleAtFixedRate(
                    createRefreshTask(
                            new IdentityRequestFilter(),
                            cacheManager.getCacheNames()), secsToMillis(initialDelay), secsToMillis(refreshPeriodSecs));

            LOGGER.info(MessageFormat.format(
                    "Cache refresh cycle (re)scheduled with period of {0} seconds.", refreshPeriodSecs));
        }
    }

    private long secsToMillis(long secs) {
        return secs * 1000;
    }

    private List<RequestMemoizingCache> getMemoizingCaches(String ... cacheNames) {

        List<RequestMemoizingCache> memoizingCaches = new ArrayList<RequestMemoizingCache>(cacheNames.length);

        for (String cacheName : cacheNames) {
            Ehcache cache = cacheManager.getEhcache(cacheName);
            if (cache instanceof RequestMemoizingCache) {
                memoizingCaches.add((RequestMemoizingCache)cache);
            }
        }

        return memoizingCaches;
    }

    private List<RefreshCycleCollaborator> getRefreshCycleCollaborators() {

        String [] cacheNames = cacheManager.getCacheNames();

        List<RefreshCycleCollaborator> refreshCycleCollaborators =
                new ArrayList<RefreshCycleCollaborator>(cacheNames.length);

        for (String cacheName : cacheNames) {
            Ehcache ehcache = cacheManager.getEhcache(cacheName);
            if (ehcache != null && ehcache instanceof RefreshCycleCollaborator) {
                refreshCycleCollaborators.add((RefreshCycleCollaborator)ehcache);
            }
        }
        return refreshCycleCollaborators;
    }

    private Set<HttpRequestAdapter> getRefreshRequestSet(String ... cacheNames) {

        Set<HttpRequestAdapter> requestSet = new LinkedHashSet<HttpRequestAdapter>();
        List<RequestMemoizingCache> memoizingCaches = getMemoizingCaches(cacheNames);

        for (RequestMemoizingCache memoizingCache : memoizingCaches) {
            requestSet.addAll(memoizingCache.getMemoizedRequestSet());
        }
        return requestSet;
    }

    private Set<String> getUpdatingCacheRegions(String ... cacheNames) {

        Set<String> updatingCacheRegions = new HashSet<String>();
        List<RequestMemoizingCache> memoizingCaches = getMemoizingCaches(cacheNames);

        for (RequestMemoizingCache memoizingCache : memoizingCaches) {
            updatingCacheRegions.add(memoizingCache.getName());
        }
        return updatingCacheRegions;
    }

    private boolean isRefreshCycleSwitchedOn() {
        return switcher.isRefreshCycleSwitchedOn();
    }

    void doRefresh(final HttpRequestFilter requestFilter, final String ... cacheNames) {

        Set<HttpRequestAdapter> requestSet = getRefreshRequestSet(cacheNames);

        Set<String> updatingCacheRegions = getUpdatingCacheRegions(cacheNames);

        if (requestFilter != null) {
            requestSet = requestFilter.filter(requestSet);
        }

        // get all the (fragment, hibernate and other) caches that will be synchronized during the refresh cycle.
        List<RefreshCycleCollaborator> refreshCycleCollaborators = getRefreshCycleCollaborators();

        try {
            for (RefreshCycleCollaborator collaborator : refreshCycleCollaborators) {
                collaborator.setIsInRefreshCycle(true, requestSet);
            }

            if (requestSet.size() > 0 && updatingCacheRegions.size() > 0) {
                requestMaker.makeRequests(requestSet, updatingCacheRegions);
            }
        }
        finally {
            for (RefreshCycleCollaborator collaborator : refreshCycleCollaborators) {
                // this allows the fragment cache to determine whether
                // keys have been missed by the refresh cycle (and evict them).
                try {
                    collaborator.setIsInRefreshCycle(false, requestSet);
                } catch(Throwable t) {
                    LOGGER.info(MessageFormat.format(
                            "Refresh cycle collaborator {0} threw exception {1} during refresh cycle termination.",
                            collaborator, t));
                }
            }
        }
    }
}
