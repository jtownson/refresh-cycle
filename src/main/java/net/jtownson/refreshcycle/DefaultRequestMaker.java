package net.jtownson.refreshcycle;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.*;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class DefaultRequestMaker implements RequestMaker {

    private static Logger LOGGER = Logger.getLogger(DefaultRequestMaker.class);
    private static final String DEFAULT_IP_ADAPTER = "0.0.0.0";

    private final String connectHost;
    private final int connectPort;
    private URLConnectionFactory urlConnectionFactory;

    public DefaultRequestMaker(String connectHost, int connectPort, URLConnectionFactory urlConnectionFactory) {
        Validate.notNull(connectHost);
        Validate.notNull(urlConnectionFactory);

        this.connectHost = defineConnectHost(connectHost);
        this.connectPort = connectPort;
        this.urlConnectionFactory = urlConnectionFactory;

        System.setProperty("sun.net.http.allowRestrictedHeaders", "true");
    }

    public boolean isConnectionAvailable() {
        try{
            Socket socket = new Socket(connectHost, connectPort);
            try {socket.close();} catch(Exception e){}
            return true;
        } catch (Exception e) {
            LOGGER.error(MessageFormat.format(
               "Cache refresh self test failed to connect to {0} on port {1} due to {2}.",
               connectHost, String.valueOf(connectPort), e.getMessage()));
            return false;
        }
    }

    public void makeRequests(
            Set<HttpRequestAdapter> requestSet, Set<String> updatingCacheRegions) {
        for (HttpRequestAdapter request : requestSet) {

            String updatingRegionsHeaderVal = join(updatingCacheRegions);
            try {
                URL url = new URL(request.getScheme(), connectHost, connectPort, request.getUri());
                try {

                    HttpURLConnection urlConnection = urlConnectionFactory.getConnection(url);

                    for (Map.Entry<String, String> header : request.getHeaders().entrySet()) {
                        urlConnection.setRequestProperty(header.getKey(), header.getValue());
                    }

                    urlConnection.setRequestProperty(RefreshConstants.REFRESH_CACHE_REGION_HEADER, updatingRegionsHeaderVal);
                    urlConnection.setRequestProperty(RefreshConstants.SYNCHRONIZE_COLLABORATING_CACHES_HEADER, "true");

                    int responseCode = urlConnection.getResponseCode();

                    if (responseCode == 200) {
                        LOGGER.debug(
                                MessageFormat.format(
                                        "Cache refresh request {0} succeeded for regions {1}.",
                                        request, updatingRegionsHeaderVal));
                    } else {
                        LOGGER.info(
                                MessageFormat.format(
                                        "Cache refresh request {0} FAILED for regions {1} with code {2}.",
                                        request, updatingRegionsHeaderVal, responseCode));
                    }
                } catch (IOException e) {
                    LOGGER.info(
                            MessageFormat.format(
                                    "Cache refresh request {0} FAILED for regions {1} due to IOException {2}.",
                                    request, updatingRegionsHeaderVal, e.getMessage()));

                }
            } catch (MalformedURLException e) {
                LOGGER.info(
                        MessageFormat.format(
                                "Cache refresh request {0} FAILED for regions {1} due to malformed url {2}.",
                                request, updatingRegionsHeaderVal, e.getMessage()));
            }

        }
    }

    public String getConnectHost() {
        return connectHost;
    }

    public int getConnectPort() {
        return connectPort;
    }

    private String join(Set<String> strings) {
        return StringUtils.join(strings, ", ");
    }

    private String defineConnectHost(String host) {
        if (DEFAULT_IP_ADAPTER.equals(host)) {
            try {
                return InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                String message =
                    "Unable to resolve hostname from default address. " +
                    "Edit server.properties, substituting 0.0.0.0 for " +
                    "the actual tomcat listen address. " + e.getMessage();
                LOGGER.error(message);
                throw new IllegalStateException(message, e);
            }
        }
        return host;
    }
}
