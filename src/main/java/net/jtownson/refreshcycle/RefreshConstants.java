package net.jtownson.refreshcycle;

/**
 *
 */
public interface RefreshConstants {
    String REFRESH_CACHE_REGION_HEADER = "x-jmt-refresh-cache-region";
    String SYNCHRONIZE_COLLABORATING_CACHES_HEADER = "x-jmt-synchronize-caches";
    String FORCE_SYNCHRONIZE_COLLABORATING_CACHES_HEADER = "x-jmt-force-synchronize-caches";
}
