package net.jtownson.refreshcycle;


import java.util.Set;

/**
 * Permit abstract of http request execution,
 * to allow for strategies of timing, threading and testing.
 */
public interface RequestMaker {

    boolean isConnectionAvailable();

    void makeRequests(
            Set<HttpRequestAdapter> requestSet,
            Set<String> updatingCacheRegions);
}
