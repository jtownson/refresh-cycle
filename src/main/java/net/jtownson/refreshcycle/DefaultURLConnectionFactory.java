package net.jtownson.refreshcycle;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class DefaultURLConnectionFactory implements URLConnectionFactory {
    public HttpURLConnection getConnection(URL url) throws IOException {
        return (HttpURLConnection)url.openConnection();
    }
}
