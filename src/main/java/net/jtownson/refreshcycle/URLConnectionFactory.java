package net.jtownson.refreshcycle;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public interface URLConnectionFactory {
    HttpURLConnection getConnection(URL url) throws IOException;
}
