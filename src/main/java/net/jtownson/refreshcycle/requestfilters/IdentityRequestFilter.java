package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;

import java.util.Set;

/**
 * Accepts everything.
 */
public class IdentityRequestFilter implements HttpRequestFilter {

    public boolean accepts(HttpRequestAdapter httpRequestAdapter) {
        return true;
    }

    public Set<HttpRequestAdapter> filter(Set<HttpRequestAdapter> requestAdapters) {
        return requestAdapters;
    }
}
