package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.apache.commons.lang.Validate;
import org.springframework.util.AntPathMatcher;

/**
 * Filters http requests based on an hostname pattern
 * (using ant path-matcher syntax with dot as the separator)
 */
public class HostNameRequestFilter extends AbstractRequestFilter {

    private final String hostPattern;
    private final AntPathMatcher pathMatcher;

    public HostNameRequestFilter(String pattern) {
        Validate.notNull(pattern, "hostname pattern cannot be null");
        pathMatcher = new AntPathMatcher();
        pathMatcher.setPathSeparator(".");
        hostPattern = pattern.toLowerCase();
    }

    public boolean accepts(HttpRequestAdapter httpRequestAdapter) {
        String hostHeader = httpRequestAdapter.getHeaders().get("host");
        return hostHeader == null ? false : pathMatcher.match(hostPattern, hostHeader.toLowerCase());
    }

}
