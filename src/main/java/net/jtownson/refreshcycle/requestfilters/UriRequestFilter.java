package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.apache.commons.lang.Validate;
import org.springframework.util.AntPathMatcher;

/**
 * Request filter that supports ant path syntax for urls
 * (e.g. /newjournal/**). Ignores the query string.
 */
public class UriRequestFilter extends AbstractRequestFilter {

    private final String uriPattern;
    private final AntPathMatcher pathMatcher;

    public UriRequestFilter(String pattern) {
        Validate.notNull(pattern, "path pattern cannot be null");
        pathMatcher = new AntPathMatcher();
        pathMatcher.setPathSeparator("/");
        uriPattern = pattern.toLowerCase();
    }

    public boolean accepts(HttpRequestAdapter httpRequestAdapter) {
        String uri = httpRequestAdapter.getUri();

        String [] components = uri.split("\\?");

        if (components.length == 0 || components[0] == null) {
            return false;
        }

        uri = components[0];

        return pathMatcher.match(uriPattern, uri);
    }

}
