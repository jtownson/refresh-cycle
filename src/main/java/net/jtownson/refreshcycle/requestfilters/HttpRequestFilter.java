package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;

import java.util.Set;

/**
 *
 */
public interface HttpRequestFilter {

    boolean accepts(HttpRequestAdapter httpRequestAdapter);
    Set<HttpRequestAdapter> filter(Set<HttpRequestAdapter> requestAdapters);
}
