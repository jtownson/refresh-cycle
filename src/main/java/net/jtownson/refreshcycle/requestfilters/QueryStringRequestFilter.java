package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.apache.commons.lang.Validate;
import org.springframework.util.AntPathMatcher;

/**
 * Does exact, case-insensitive substring matching on the query string
 * (e.g. "vol=9&article_id=abc123")
 */
public class QueryStringRequestFilter extends AbstractRequestFilter {

    private final String queryPattern;
    private final AntPathMatcher pathMatcher;

    public QueryStringRequestFilter(String pattern) {
        Validate.notNull(pattern, "test pattern cannot be null");
        this.queryPattern = pattern.toLowerCase();
        pathMatcher = new AntPathMatcher();
        pathMatcher.setPathSeparator("&");
    }

    public boolean accepts(HttpRequestAdapter httpRequestAdapter) {
        String uri = httpRequestAdapter.getUri();
        String [] components = uri.split("\\?");

        if (components.length != 2 || components[1] == null) {
            return false;
        }

        String queryString = components[1].toLowerCase();

        return pathMatcher.match(queryPattern, queryString);
    }
}
