package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 */
public abstract class AbstractRequestFilter implements HttpRequestFilter {

    public Set<HttpRequestAdapter> filter(Set<HttpRequestAdapter> requestAdapters) {

        Set<HttpRequestAdapter> filteredRequestAdapters =
                new LinkedHashSet<HttpRequestAdapter>(requestAdapters.size());

        for(HttpRequestAdapter requestAdapter : requestAdapters) {
            if (accepts(requestAdapter)) {
                filteredRequestAdapters.add(requestAdapter);
            }
        }
        return filteredRequestAdapters;
    }
}
