package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;

/**
 * Combine several request filters.
 */
public class CompositeRequestFilter extends AbstractRequestFilter {

    private final HttpRequestFilter[] requestFilters;

    public CompositeRequestFilter(HttpRequestFilter ... filters) {
        requestFilters = filters;
    }

    public boolean accepts(HttpRequestAdapter httpRequestAdapter) {
        for(HttpRequestFilter filter : requestFilters) {
            if ( filter != null && ! filter.accepts(httpRequestAdapter)) {
                return false;
            }
        }
        return true;
    }
}
