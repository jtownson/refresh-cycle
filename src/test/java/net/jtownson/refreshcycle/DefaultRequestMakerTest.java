package net.jtownson.refreshcycle;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 *
 */
public class DefaultRequestMakerTest {

    private static final String REQUEST_URI = "/newjournal/index.html";
    private static final String HOST = "127.0.0.1";
    private static final String METHOD = "GET";
    private static final String PROTOCOL = "http";
    private static final int PORT = 8080;
    @Mock
    private URLConnectionFactory urlConnectionFactory;

    @Mock
    private HttpURLConnection urlConnection;

    private DefaultRequestMaker requestMaker;
    private URL url;
    private MockHttpServletRequest request = getOriginalMemoizedClientRequest();

    @Before
    public void setup() throws Exception {
        initMocks(this);
        requestMaker = new DefaultRequestMaker(HOST, PORT, urlConnectionFactory);
        url = new URL(PROTOCOL, HOST, PORT, REQUEST_URI);
    }

    @Test
    public void shouldConvertDefaultNetAddress() throws UnknownHostException {

        // given
        requestMaker = new DefaultRequestMaker("0.0.0.0", PORT, urlConnectionFactory);

        // when
        String host = requestMaker.getConnectHost();

        // then
        assertThat(host, is(InetAddress.getLocalHost().getHostName()));
    }

    @Test
    public void shouldMakeCorrectRequest() throws Exception {

        // given
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter(request, Collections.EMPTY_LIST, Collections.EMPTY_SET);
        Set<HttpRequestAdapter> requestSet = new HashSet<HttpRequestAdapter>(Arrays.asList(requestAdapter));

        // when
        when(urlConnectionFactory.getConnection(url)).thenReturn(urlConnection);
        requestMaker.makeRequests(requestSet, Collections.EMPTY_SET);

        // then
        verify(urlConnectionFactory).getConnection(url);
        verify(urlConnection).getResponseCode();
    }

    @Test
    public void shouldSetRefreshingCacheRegionsHeader() throws Exception {

        // given
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter(request, Collections.EMPTY_LIST, Collections.EMPTY_SET);
        Set<HttpRequestAdapter> requestSet = new HashSet<HttpRequestAdapter>(Arrays.asList(requestAdapter));
        Set<String> updatingRegions = new LinkedHashSet<String>(Arrays.asList("region-1", "region-2"));

        // when
        when(urlConnectionFactory.getConnection(url)).thenReturn(urlConnection);
        requestMaker.makeRequests(requestSet, updatingRegions);

        // then
        verify(urlConnection).setRequestProperty("x-jmt-refresh-cache-region", "region-1, region-2");
    }

    @Test
    public void shouldSetSynchronizeCacheHeader() throws Exception {

        // given
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter(request, Collections.EMPTY_LIST, Collections.EMPTY_SET);
        Set<HttpRequestAdapter> requestSet = new HashSet<HttpRequestAdapter>(Arrays.asList(requestAdapter));
        Set<String> updatingRegions = new LinkedHashSet<String>(Arrays.asList("region-1", "region-2"));

        // when
        when(urlConnectionFactory.getConnection(url)).thenReturn(urlConnection);
        requestMaker.makeRequests(requestSet, updatingRegions);

        // then
        verify(urlConnection).setRequestProperty("x-jmt-synchronize-caches", "true");

    }

    @Test
    public void shouldSetOnlyHeadersDefinedInRestrictionSet() throws Exception {

        // given
        Set<String> refreshRequestHeaders = new LinkedHashSet<String>(Arrays.asList("host"));
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter(request, Collections.EMPTY_LIST, refreshRequestHeaders);
        Set<HttpRequestAdapter> requestSet = new HashSet<HttpRequestAdapter>(Arrays.asList(requestAdapter));

        // when
        when(urlConnectionFactory.getConnection(url)).thenReturn(urlConnection);
        requestMaker.makeRequests(requestSet, Collections.EMPTY_SET);

        // then
        verify(urlConnection, times(3)).setRequestProperty(anyString(), anyString());
        verify(urlConnection).setRequestProperty("x-jmt-refresh-cache-region", "");
        verify(urlConnection).setRequestProperty("host", "my-host");
        verify(urlConnection).setRequestProperty("x-jmt-synchronize-caches", "true");
    }

    @Test
    public void shouldLogSuccessfulRequestAtDebug() throws Exception {

        // given
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter(request, Collections.EMPTY_LIST, Collections.EMPTY_SET);
        Set<HttpRequestAdapter> requestSet = new HashSet<HttpRequestAdapter>(Arrays.asList(requestAdapter));
        Logger logger = mock(Logger.class);
        ReflectionTestUtils.setField(requestMaker, "LOGGER", logger, Logger.class);

        // when
        when(urlConnectionFactory.getConnection(url)).thenReturn(urlConnection);
        when(urlConnection.getResponseCode()).thenReturn(200);

        requestMaker.makeRequests(requestSet, Collections.EMPTY_SET);

        // then
        verify(logger, times(1)).debug(anyString());
        verify(logger, never()).info(anyString());
        verify(logger, never()).warn(anyString());
        verify(logger, never()).error(anyString());
    }

    @Test
    public void shouldLogFailedRequestAtInfo() throws Exception {

        // given
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter(request, Collections.EMPTY_LIST, Collections.EMPTY_SET);
        Set<HttpRequestAdapter> requestSet = new HashSet<HttpRequestAdapter>(Arrays.asList(requestAdapter));
        Logger logger = mock(Logger.class);
        ReflectionTestUtils.setField(requestMaker, "LOGGER", logger, Logger.class);

        // when
        when(urlConnectionFactory.getConnection(url)).thenReturn(urlConnection);
        when(urlConnection.getResponseCode()).thenReturn(500);


        requestMaker.makeRequests(requestSet, Collections.EMPTY_SET);

        // then
        verify(logger, never()).debug(anyString());
        verify(logger, times(1)).info(anyString());
        verify(logger, never()).warn(anyString());
        verify(logger, never()).error(anyString());
    }

    @Test
    public void shouldLCatchAndLogIOExceptionsAtInfo() throws Exception {

        // given
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter(request, Collections.EMPTY_LIST, Collections.EMPTY_SET);
        Set<HttpRequestAdapter> requestSet = new HashSet<HttpRequestAdapter>(Arrays.asList(requestAdapter));
        Logger logger = mock(Logger.class);
        ReflectionTestUtils.setField(requestMaker, "LOGGER", logger, Logger.class);

        // when
        when(urlConnectionFactory.getConnection(url)).thenReturn(urlConnection);
        when(urlConnection.getResponseCode()).thenThrow(new IOException("Boo!"));

        requestMaker.makeRequests(requestSet, Collections.EMPTY_SET);

        // then
        verify(logger, never()).debug(anyString());
        verify(logger, times(1)).info(anyString());
        verify(logger, never()).warn(anyString());
        verify(logger, never()).error(anyString());
    }

    @Test
    public void shouldLCatchAndLogUrlExceptionsAtInfo() throws Exception {

        // given
        request.setScheme("httppps");
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter(request, Collections.EMPTY_LIST, Collections.EMPTY_SET);
        Set<HttpRequestAdapter> requestSet = new HashSet<HttpRequestAdapter>(Arrays.asList(requestAdapter));
        Logger logger = mock(Logger.class);
        ReflectionTestUtils.setField(requestMaker, "LOGGER", logger, Logger.class);

        // when
        requestMaker.makeRequests(requestSet, Collections.EMPTY_SET);

        // then
        verify(logger, never()).debug(anyString());
        verify(logger, times(1)).info(anyString());
        verify(logger, never()).warn(anyString());
        verify(logger, never()).error(anyString());
    }

    private MockHttpServletRequest getOriginalMemoizedClientRequest() {
        MockHttpServletRequest request = new MockHttpServletRequest(METHOD, REQUEST_URI);
        request.addHeader("host", "my-host");
        return request;
    }
}
