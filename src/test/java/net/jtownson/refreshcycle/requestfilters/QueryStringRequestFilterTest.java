package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.junit.Test;

import static junit.framework.Assert.fail;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class QueryStringRequestFilterTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldInvalidateNullSubString() {

        // given
        String queryPattern = null;

        // when
        QueryStringRequestFilter filter = new QueryStringRequestFilter(queryPattern);

        // then
        fail("Should not have reached this point");
    }

    @Test
    public void shouldMatchCaseInsensitive() {

        // given
        String queryPattern = "**&article_id=gb-2011-12-10-230&**";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.setUri("/fullText.html?key=val&article_id=gb-2011-12-10-230");

        // when
        QueryStringRequestFilter filter = new QueryStringRequestFilter(queryPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(true));
    }
}
