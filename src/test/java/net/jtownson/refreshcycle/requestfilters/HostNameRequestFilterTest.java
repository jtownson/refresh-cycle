package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.junit.Test;

import static junit.framework.Assert.fail;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


/**
 *
 */
public class HostNameRequestFilterTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldInvalidateNullPatterns() {

        // given
        String hostPattern = null;

        // when
        HostNameRequestFilter filter = new HostNameRequestFilter(hostPattern);

        // then
        fail("Should not have reached this point");
    }

    @Test
    public void shouldMatchAnExactMatch() {

        // given
        String hostPattern = "www.biomedcentral.com";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.addHeader("HOST", "www.biomedcentral.com");

        // when
        HostNameRequestFilter filter = new HostNameRequestFilter(hostPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(true));
    }

    @Test
    public void shouldIgnoreCaseOfHeaderValueWhenMatching() {

        // given
        String hostPattern = "www.biomedcentral.com";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.addHeader("HOST", "WWW.BIOMEDCENTRAL.COM");

        // when
        HostNameRequestFilter filter = new HostNameRequestFilter(hostPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(true));
    }

    @Test
    public void shouldIgnoreCaseOfHeaderNameWhenMatching() {

        // given
        String hostPattern = "www.biomedcentral.com";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.addHeader("hOsT", "www.biomedcentral.com");

        // when
        HostNameRequestFilter filter = new HostNameRequestFilter(hostPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(true));
    }

    @Test
    public void shouldSupportStarSuffixForIgnoringHostName() {

        // given
        String hostPattern = "*.biomedcentral.com";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.addHeader("host", "www.biomedcentral.com");

        // when
        HostNameRequestFilter filter = new HostNameRequestFilter(hostPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(true));
    }

    @Test
    public void shouldSupportDoubleStarForMatchingAll() {

        // given
        String hostPattern = "**";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.addHeader("host", "www.biomedcentral.com");

        // when
        HostNameRequestFilter filter = new HostNameRequestFilter(hostPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(true));
    }

    @Test
    public void shouldSupportThreeStarsForMatchingAll() {

        // given
        String hostPattern = "*.*.*";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.addHeader("host", "www.biomedcentral.com");

        // when
        HostNameRequestFilter filter = new HostNameRequestFilter(hostPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(true));
    }

    @Test
    public void shouldNotMatchWrongDomain() {

        // given
        String hostPattern = "www.biomedcentral.com";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.addHeader("host", "www.genomemedicine.com");

        // when
        HostNameRequestFilter filter = new HostNameRequestFilter(hostPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(false));
    }

}
