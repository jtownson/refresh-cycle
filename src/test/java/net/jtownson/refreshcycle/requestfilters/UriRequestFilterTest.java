package net.jtownson.refreshcycle.requestfilters;

import net.jtownson.refreshcycle.HttpRequestAdapter;
import org.junit.Test;

import static junit.framework.Assert.fail;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class UriRequestFilterTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldInvalidateNullPatterns() {

        // given
        String uriPattern = null;

        // when
        UriRequestFilter filter = new UriRequestFilter(uriPattern);

        // then
        fail("Should not have reached this point");
    }

    @Test
    public void shouldMatchAPathWithAStar() {

        // given
        String uriPattern = "/newjournal/*";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.setUri("/newjournal/index.html");

        // when
        UriRequestFilter filter = new UriRequestFilter(uriPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(true));
    }

    @Test
    public void shouldNotMatchAPathWithAStarWhenTwoStarsNeeded() {

        // given
        String uriPattern = "/newjournal/*";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.setUri("/newjournal/admin/index.html");

        // when
        UriRequestFilter filter = new UriRequestFilter(uriPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(false));
    }

    @Test
    public void shouldMatchCaseSensitive() {

        // given
        String uriPattern = "/newjournal/*";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.setUri("/NEWJOURNAL/index.html");

        // when
        UriRequestFilter filter = new UriRequestFilter(uriPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(false));
    }

    @Test
    public void shouldIgnoreTheQueryString() {

        // given
        String uriPattern = "/newjournal/browse.html";
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.setUri("/newjournal/browse.html?page=1&itemsPerPage=25");

        // when
        UriRequestFilter filter = new UriRequestFilter(uriPattern);

        // then
        assertThat(filter.accepts(requestAdapter), is(true));
    }
}
