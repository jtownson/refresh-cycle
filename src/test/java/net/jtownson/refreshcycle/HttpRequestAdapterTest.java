package net.jtownson.refreshcycle;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 *
 */
public class HttpRequestAdapterTest {

    @Test
    public void shouldNormalizeHeaderNamesCorrectly() {

        // given
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();

        // when
        requestAdapter.addHeader("user-agent", "Firefox");

        // then
        assertThat(requestAdapter.getHeaders().get("USER-AGENT"), is("Firefox"));
        assertThat(requestAdapter.getHeaders().keySet().iterator().next(), is("User-Agent"));
    }

    @Test
    public void shouldMakeToStringPrettyEnough() {

        // given
        HttpRequestAdapter requestAdapter = new HttpRequestAdapter();
        requestAdapter.setMethod("GET");
        requestAdapter.setScheme("http");
        requestAdapter.addHeader("host", "www.genomebiology.com");
        requestAdapter.setUri("/newjournal/browse.html?page=1&itemsPerPage=25");

        // when
        String s = requestAdapter.toString();

        // then
        assertThat(s, isPretty());
    }

    private static Matcher<Object> isPretty() {

        return new BaseMatcher<Object>() {
            public boolean matches(Object o) {
                return "http://www.genomebiology.com/newjournal/browse.html?page=1&itemsPerPage=25".equals(o);
            }

            public void describeTo(Description description) {}
        };
    }
}
