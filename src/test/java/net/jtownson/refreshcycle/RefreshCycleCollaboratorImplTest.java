package net.jtownson.refreshcycle;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.loader.CacheLoader;
import org.apache.log4j.MDC;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 */
public class RefreshCycleCollaboratorImplTest {

    private RefreshCycleCollaboratorImpl cacheDecorator;

    @Mock
    private Ehcache cache;
    @Mock
    private Element element;
    @Mock
    private CacheLoader cacheLoader;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        cacheDecorator = new RefreshCycleCollaboratorImpl(cache);
    }

    @After
    public void tearDown() {
        endRefreshRequest();
    }

    @Test
    public void getShouldReturnNullIfInRefreshCycleAndKeyNotAlreadyRefreshed() {

        // given
        makeRefreshRequest();
        String aKey = "key";

        // when
        cacheDecorator.setIsInRefreshCycle(true, null);
        cacheDecorator.get(aKey);
        cacheDecorator.setIsInRefreshCycle(false, null);

        // then
        verify(cache, times(0)).get(aKey);
    }

    @Test
    public void getShouldNotReturnNullInRefreshCycleIfKeyAlreadyRefreshedInThisCycle() {

        // given
        makeRefreshRequest();
        String aKey = "key";
        Element anElement = new Element(aKey, "aValue");

        // when
        cacheDecorator.setIsInRefreshCycle(true, null);
        cacheDecorator.put(anElement);
        cacheDecorator.get(aKey);
        cacheDecorator.setIsInRefreshCycle(false, null);

        // then
        verify(cache, times(1)).get(aKey);
    }

    @Test
    public void getShouldReturnNullWhenGettingAnotherKey() {

        // given
        makeRefreshRequest();
        String aKey = "key";
        Element anElement = new Element(aKey, "aValue");

        // when
        cacheDecorator.setIsInRefreshCycle(true, null);
        cacheDecorator.put(anElement);
        cacheDecorator.get("another_key");
        cacheDecorator.setIsInRefreshCycle(false, null);

        // then
        verify(cache, times(0)).get(aKey);
    }

    @Test
    public void getShouldReturnNullWhenForceHeaderIsSet() {

        // given
        String aKey = "key";
        Element anElement = new Element(aKey, "aValue");
        cacheDecorator.put(anElement);

        // when
        makeRefreshRequestWithForceHeader();
        Element e = cacheDecorator.get(aKey);

        // then
        assertTrue(e == null);
        verify(cache, times(0)).get(aKey);
    }

    private static void makeRefreshRequest() {
        MockHttpServletRequest request = new MockHttpServletRequest("get", "index.html");
        request.addHeader(RefreshConstants.SYNCHRONIZE_COLLABORATING_CACHES_HEADER, "true");
        MDC.put(RefreshCycleFilter.HTTP_REQUEST, request);
    }

    private static void makeRefreshRequestWithForceHeader() {
        MockHttpServletRequest request = new MockHttpServletRequest("get", "index.html");
        request.addHeader(RefreshConstants.FORCE_SYNCHRONIZE_COLLABORATING_CACHES_HEADER, "true");
        MDC.put(RefreshCycleFilter.HTTP_REQUEST, request);
    }


    private static void endRefreshRequest() {
        MDC.remove(RefreshCycleFilter.HTTP_REQUEST);
    }

}
