package net.jtownson.refreshcycle.spring;

import net.jtownson.refreshcycle.RefreshCycleSwitcher;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.constructs.EhcacheDecoratorAdapter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 */
public class SwitchingEhcacheFactoryBeanTest {

    static class MyFactoryImpl extends SwitchingEhcacheFactoryBean {

        @Override
        protected Ehcache decorateStep(Ehcache cache) {
            return new MyDecoratorImpl(cache);
        }
    }

    static class MyDecoratorImpl extends EhcacheDecoratorAdapter {

        public MyDecoratorImpl(Ehcache underlyingCache) {
            super(underlyingCache);
        }
    }

    private Ehcache cache;
    private SwitchingEhcacheFactoryBean cacheFactory;
    private RefreshCycleSwitcher switcher;
    
    @Before
    public void setup() {
        cache = mock(Ehcache.class);
        cacheFactory = new MyFactoryImpl();
        switcher = mock(RefreshCycleSwitcher.class);
        
    }

    @Test
    public void shouldReturnDecoratedCacheWhenSwitcherIsOn() {

        // given
        cacheFactory.setSwitcher(switcher);
        when(switcher.isRefreshCycleSwitchedOn()).thenReturn(true);
        
        // when
        Ehcache decoratedCache = cacheFactory.decorateCache(cache);

        // then
        assertTrue(decoratedCache instanceof MyDecoratorImpl);
    }

    @Test
    public void shouldReturnUndecoratedCacheWhenSwitcherIsOff() {

        // given
        cacheFactory.setSwitcher(switcher);
        when(switcher.isRefreshCycleSwitchedOn()).thenReturn(false);

        // when
        Ehcache decoratedCache = cacheFactory.decorateCache(cache);

        // then
        assertFalse(decoratedCache instanceof MyDecoratorImpl);
        assertTrue(decoratedCache == cache);
    }

    @Test
    public void shouldReturnUndecoratedCacheWhenSwitcherIsMissing() {

        // given
        cacheFactory.setSwitcher(null);

        // when
        Ehcache decoratedCache = cacheFactory.decorateCache(cache);

        // then
        assertFalse(decoratedCache instanceof MyDecoratorImpl);
        assertTrue(decoratedCache == cache);
    }

}
