package net.jtownson.refreshcycle.keybuilders;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.Cookie;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;

/**
 *
 */
public class CookieKeyBuilderTest {

    @Test
    public void testCookieHashingBMCUsrIdCookie() {

        // given
        Cookie cookie = new Cookie(
                "bmccookie",
                "lname=Parkin&email=emma.parkin%40biomedcentral.com&refreshdate=4+Mar+2011&usrlastvisit=4+Aug+2010&clientip=10.0.14.21&fname=Emma&usrid=3322714291770266");

        cookie.setPath("/");

        CookieKeyBuilder keyBuilder = new CookieKeyBuilder("/", "bmccookie", "usrid=\\d+");

        MockHttpServletRequest request = new MockHttpServletRequest("GET", "index.html");
        request.setCookies(cookie);

        String expectedKey = "bmccookie" + "/" + "usrid=3322714291770266";

        // when
        StringBuilder stringBuilder = new StringBuilder();
        keyBuilder.appendToKey(request, stringBuilder);
        String actualKey = stringBuilder.toString();

        // then
        assertEquals(expectedKey, actualKey);
    }

    @Test
    public void testCookieHashingWhenMultipleExpressionsInBMCUsrIdAndInstitutionIdInCookie() {

        // given
        Cookie cookie = new Cookie(
                "bmccookie",
                "institution_linkout=&institution_sfx_image_link=http%3A%2F%2Fwww.exlibrisgroup.com%2Ffiles%2FProducts%2FSFX%2Fsfxbutton.gif&institution_sfx_link=http%3A%2F%2Fsfx-demo.exlibrisgroup.com%3A3210%2Fdemo&institution_name=BioMed+Central&institution_ip_address=80.169.130.178&instcode=201&institution_id=201&institution_last_refreshed=1305623190153&lname=Parkin&email=emma.parkin%40biomedcentral.com&refreshdate=4+Mar+2011&usrlastvisit=4+Aug+2010&clientip=10.0.14.21&fname=Emma&usrid=3322714291770266");

        cookie.setPath("/");

        CookieKeyBuilder keyBuilder = new CookieKeyBuilder("/", "bmccookie", Arrays.asList("usrid=\\d+", "institution_id=\\d+"));

        MockHttpServletRequest request = new MockHttpServletRequest("GET", "index.html");
        request.setCookies(cookie);

        String expectedKey = "bmccookie" + "/" + "usrid=3322714291770266" + "institution_id=201";

        // when
        StringBuilder stringBuilder = new StringBuilder();
        keyBuilder.appendToKey(request, stringBuilder);
        String actualKey = stringBuilder.toString();

        // then
        assertEquals(expectedKey, actualKey);
    }

}
