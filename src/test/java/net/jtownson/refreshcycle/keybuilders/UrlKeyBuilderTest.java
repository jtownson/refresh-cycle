package net.jtownson.refreshcycle.keybuilders;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class UrlKeyBuilderTest {

    @Test
    public void shouldStripLeadingAmpersandToOvercomeTomcatHttpBug() {

        // given
        MockHttpServletRequest request = new MockHttpServletRequest("get", "/browse.html");
        request.setQueryString("&page=2&itemsPerPage=25");

        // when
        String url = UrlKeyBuilder.getUrl(request);

        // then
        assertThat(url, is("/browse.html?page=2&itemsPerPage=25"));
    }

}
