package net.jtownson.refreshcycle.keybuilders;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

import static net.jtownson.refreshcycle.keybuilders.UtilityFunctions.asHashSet;
import static junit.framework.Assert.assertEquals;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class RequestMethodKeyBuilderTest {

    @Test
    public void shouldGiveDifferentHashesForDifferentRequestMethods() {

        // given
        RequestMethodKeyBuilder keyBuilder = new RequestMethodKeyBuilder();
        HttpServletRequest getRequest = new MockHttpServletRequest("GET", null);
        HttpServletRequest putRequest = new MockHttpServletRequest("PUT", null);
        HttpServletRequest postRequest = new MockHttpServletRequest("POST", null);
        HttpServletRequest delRequest = new MockHttpServletRequest("DELETE", null);


        // when
        Set<String> keySet = asHashSet(
                generateKey(keyBuilder, getRequest),
                generateKey(keyBuilder, putRequest),
                generateKey(keyBuilder, postRequest),
                generateKey(keyBuilder, delRequest));

        // then
        assertThat(keySet.size(), is(4));

    }

    @Test
    public void shouldGiveSameHashForSameMethod() {
        // given
        RequestMethodKeyBuilder keyBuilder = new RequestMethodKeyBuilder();
        HttpServletRequest getRequest = new MockHttpServletRequest("GET", null);
        HttpServletRequest anotherGetRequest = new MockHttpServletRequest("GET", null);

        // then
        assertEquals(generateKey(keyBuilder, getRequest), generateKey(keyBuilder, anotherGetRequest));
    }

    @Test
    public void shouldGiveHashCodeThatIsCaseInsensitive() {
        // given
        RequestMethodKeyBuilder keyBuilder = new RequestMethodKeyBuilder();
        HttpServletRequest getRequest = new MockHttpServletRequest("GET", null);
        HttpServletRequest anotherGetRequest = new MockHttpServletRequest("get", null);

        // then
        assertEquals(generateKey(keyBuilder, getRequest), generateKey(keyBuilder, anotherGetRequest));
    }

    private String generateKey(RequestMethodKeyBuilder keyBuilder, HttpServletRequest getRequest) {
        StringBuilder stringBuilder = new StringBuilder();
        keyBuilder.appendToKey(getRequest, stringBuilder);
        return stringBuilder.toString();
    }

}
