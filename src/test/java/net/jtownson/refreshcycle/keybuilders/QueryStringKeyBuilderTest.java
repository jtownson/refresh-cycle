package net.jtownson.refreshcycle.keybuilders;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
/**
 *
 */
public class QueryStringKeyBuilderTest {

    @Test
    public void shouldGetTheSameHashCodeForTheSameURLParams() {

        // given
        HttpServletRequest request = getRequestWithParams(2);
        HttpServletRequest anotherRequest = getRequestWithParams(2);

        UrlKeyBuilder keyBuilder = new UrlKeyBuilder();

        // then
        assertThat(generateKey(request, keyBuilder), is(generateKey(anotherRequest, keyBuilder)));
    }

    @Test
    public void shouldGetDifferentHashCodeForDifferentURLParams() {

        // given
        HttpServletRequest request = getRequestWithParams(2, "A");
        HttpServletRequest anotherRequest = getRequestWithParams(2, "B");

        UrlKeyBuilder keyBuilder = new UrlKeyBuilder();

        // then
        assertThat(generateKey(request, keyBuilder), not(generateKey(anotherRequest, keyBuilder)));
    }

    @Test
    public void shouldGetSameHashCodeWhenURLParamsInDifferentOrder() {
        // given
        HttpServletRequest request = getRequestWithParams(2, "A", false);
        HttpServletRequest anotherRequest = getRequestWithParams(2, "A", true);

        UrlKeyBuilder keyBuilder = new UrlKeyBuilder();

        // then
        assertThat(generateKey(request, keyBuilder), is(generateKey(anotherRequest, keyBuilder)));
    }

    @Test
    public void hashCodesShouldBeCaseSensitive() {
        // given
        HttpServletRequest request = getRequestWithParams(2, "a", false);
        HttpServletRequest anotherRequest = getRequestWithParams(2, "A", true);

        UrlKeyBuilder keyBuilder = new UrlKeyBuilder();

        // then
        assertThat(generateKey(request, keyBuilder), not(generateKey(anotherRequest, keyBuilder)));
    }

    private String generateKey(HttpServletRequest request, UrlKeyBuilder keyBuilder) {
        StringBuilder stringBuilder = new StringBuilder();
        keyBuilder.appendToKey(request, stringBuilder);
        return stringBuilder.toString();
    }

    private MockHttpServletRequest getRequestWithParams(int numParams) {
        return getRequestWithParams(numParams, "");
    }

    private MockHttpServletRequest getRequestWithParams(int numParams, String prefix) {
        return getRequestWithParams(numParams, prefix, false);
    }

    private MockHttpServletRequest getRequestWithParams(int numParams, String prefix, boolean reverse) {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "page");
        Map<String, String[]> parameterMap = new LinkedHashMap<String, String[]>();

        if (reverse) {
            for (int i = numParams-1; i >= 0; i--) {
                parameterMap.put("param"+i, new String[]{prefix + "val"+i});
            }

        } else {
            for (int i = 0; i < numParams; i++) {
                parameterMap.put("param"+i, new String[]{prefix + "val"+i});
            }
        }

        request.setParameters(parameterMap);
        return request;
    }

}
