package net.jtownson.refreshcycle.keybuilders;

import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class HostHeaderKeyBuilderTest extends HeaderKeyBuilderTest {

    static final String HOST = "Host";

    @Test
    public void shouldGiveDifferentHashesForDifferentHosts() {

        // given
        HostHeaderKeyBuilder keyBuilder = new HostHeaderKeyBuilder();
        HttpServletRequest request = getRequest(HOST, "www.genomemedicine.com");
        HttpServletRequest anotherRequest = getRequest(HOST, "www.genomebiology.com");

        // when
        Set<String> keySet = UtilityFunctions.asHashSet(
                generateKey(keyBuilder, request),
                generateKey(keyBuilder, anotherRequest));

        // then
        assertThat(keySet.size(), is(2));

    }

    @Test
    public void shouldGiveSameHashForSameMethod() {

        // given
        HostHeaderKeyBuilder keyBuilder = new HostHeaderKeyBuilder();
        HttpServletRequest request = getRequest(HOST, "www.genomemedicine.com");
        HttpServletRequest anotherRequest = getRequest(HOST, "www.genomemedicine.com");

        // then
        assertEquals(generateKey(keyBuilder, request), generateKey(keyBuilder, anotherRequest));
    }

    @Test
    public void shouldGiveHashCodeThatIsCaseInsensitive() {
        // given
        HostHeaderKeyBuilder keyBuilder = new HostHeaderKeyBuilder();
        HttpServletRequest request = getRequest(HOST, "www.genomemedicine.com");
        HttpServletRequest anotherRequest = getRequest(HOST, "WWW.GENOMEMEDICINE.COM");

        // then
        assertEquals(generateKey(keyBuilder, request), generateKey(keyBuilder, anotherRequest));
    }

}
