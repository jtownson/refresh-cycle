package net.jtownson.refreshcycle.keybuilders;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class UtilityFunctions {

    static <T> Set<T> asHashSet(T... elements) {
        Set<T> result = new HashSet<T>();
        result.addAll(Arrays.asList(elements));
        return result;
    }

}
