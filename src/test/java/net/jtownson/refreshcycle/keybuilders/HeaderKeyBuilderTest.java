package net.jtownson.refreshcycle.keybuilders;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

import static net.jtownson.refreshcycle.keybuilders.UtilityFunctions.asHashSet;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class HeaderKeyBuilderTest {

    @Test
    public void shouldGiveDifferentHashesForDifferentHeaderValuesButSameHeaderName() {

        // given
        HeaderKeyBuilder keyBuilder = new HeaderKeyBuilder("urlmapped");
        HttpServletRequest request = getRequest("urlmapped", "/content");
        HttpServletRequest anotherRequest = getRequest("urlmapped", "/");

        // when
        Set<String> keySet = asHashSet(
                generateKey(keyBuilder, request),
                generateKey(keyBuilder, anotherRequest));

        // then
        assertThat(keySet.size(), is(2));
    }

    @Test
    public void shouldGiveDifferentHashesForSameHeaderValuesButDifferentHeaderName() {

        // given
        HeaderKeyBuilder urlMappedKeyBuilder = new HeaderKeyBuilder("urlmapped");
        HttpServletRequest urlMappedRequest = getRequest("urlmapped", "foo");

        HeaderKeyBuilder hostKeyBuilder = new HeaderKeyBuilder("Host");
        HttpServletRequest hostRequest = getRequest("Host", "foo");

        // when
        Set<String> keySet = asHashSet(
                generateKey(hostKeyBuilder, hostRequest),
                generateKey(urlMappedKeyBuilder, urlMappedRequest));

        // then
        assertThat(keySet.size(), is(2));
    }

    protected HttpServletRequest getRequest(String headerName, String headerValue) {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", null);
        request.addHeader(headerName, headerValue);
        return request;
    }

    protected String generateKey(KeyBuilder keyBuilder, HttpServletRequest getRequest) {
        StringBuilder stringBuilder = new StringBuilder();
        keyBuilder.appendToKey(getRequest, stringBuilder);
        return stringBuilder.toString();
    }

}
