package net.jtownson.refreshcycle.management;

import net.jtownson.refreshcycle.*;
import net.jtownson.refreshcycle.keybuilders.KeyBuilder;
import net.jtownson.refreshcycle.keybuilders.UrlKeyBuilder;
import net.jtownson.refreshcycle.requestfilters.IdentityRequestFilter;
import net.jtownson.refreshcycle.RequestMaker;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.log4j.MDC;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockHttpServletRequest;

import java.util.Arrays;
import java.util.Set;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;


/**
 *
 */
@SuppressWarnings("unchecked")
public class RefreshCycleManagerTest {

    private RequestMaker requestMaker;
    private RefreshCycleSwitcher switcher;
    private CacheManager cacheManager;
    private Ehcache cache;
    private RefreshCycleManager cacheRefresh;

    @Before
    public void setup() throws Exception {
        cacheManager = CacheManager.create(new ClassPathResource("ehcache-failsafe.xml").getURL());
        cacheManager.removalAll();
        cache = getACache();
        cacheManager.addCache(cache);

        // simulate a request
        MDC.put(RefreshCycleFilter.HTTP_REQUEST, new MockHttpServletRequest("GET", "index.html"));
        cache.put(new Element("fragment_key", "<div>fragment</div>"));

        requestMaker = mock(RequestMaker.class);
        switcher = mock(RefreshCycleSwitcher.class);
    }

    @After
    public void teardown() {
        cacheManager.shutdown();
        if (cacheRefresh != null) {
            cacheRefresh.cancelTimer();
        }
        MDC.remove(RefreshCycleFilter.HTTP_REQUEST);
    }

    @Test
    public void shouldInitializeIfFeatureDisabledEvenWhenSelfTestFails() {
        // given
        when(requestMaker.isConnectionAvailable()).thenReturn(false);
        when(switcher.isRefreshCycleSwitchedOn()).thenReturn(false);

        // when
        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 0L);

        // then fine.
    }

    @Test
    public void refreshShouldMakeTheRightRequests() {
        // given
        // initialize objects
        when(requestMaker.isConnectionAvailable()).thenReturn(true);
        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 0L);

        // when
        cacheRefresh.doRefresh(new IdentityRequestFilter(), cacheManager.getCacheNames());

        // then
        ArgumentCaptor<Set> arg1 = ArgumentCaptor.forClass(Set.class);
        ArgumentCaptor<Set> arg2 = ArgumentCaptor.forClass(Set.class);
        verify(requestMaker).makeRequests(arg1.capture(), arg2.capture());
        Set<HttpRequestAdapter> requestSet = arg1.getValue();
        Set<String> updatingReqions = arg2.getValue();

        assertThat(requestSet.size(), is(1));
        HttpRequestAdapter requestAdapter = (HttpRequestAdapter)requestSet.toArray()[0];
        assertThat(requestAdapter.getUri(), is("index.html"));

        assertThat(updatingReqions.size(), is(1));
        String region = (String)updatingReqions.toArray()[0];
        assertThat(region, is("acache"));
    }

    @Test
    public void shouldShowTheCorrectRefreshRequestsToJConsole() {

        // given
        // initialize objects
        when(requestMaker.isConnectionAvailable()).thenReturn(true);
        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 0L);

        // when
        String requests = cacheRefresh.showRefreshRequests(null, null, null);

        // then
        assertThat(requests, containsString("index.html"));
    }

    @Test
    public void shouldSetRunningToTrueWhenRunning() throws Exception {
        // given
        when(requestMaker.isConnectionAvailable()).thenReturn(true);

        Answer waitAnswer = new Answer() {
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                // just block until the parent test is done.
                Thread.currentThread().join();
                return null;
            }
        };

        doAnswer(waitAnswer).when(requestMaker).makeRequests(Matchers.<Set<HttpRequestAdapter>>any(), Matchers.<Set<String>>any());

        when(switcher.isRefreshCycleSwitchedOn()).thenReturn(true);
        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 0L);

        // when
        cacheRefresh.refresh();
        Thread.sleep(10);

        // then
        assertThat(cacheRefresh.isRunning(), is(true));
    }

    @Test
    public void shouldNotSetRunningToTrueWhenRunning() throws Exception {
        // given
        when(requestMaker.isConnectionAvailable()).thenReturn(true);
        when(switcher.isRefreshCycleSwitchedOn()).thenReturn(false);

        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 0L);

        // when
        cacheRefresh.refresh();
        Thread.sleep(10);

        // then
        verify(requestMaker, never()).makeRequests(Matchers.<Set<HttpRequestAdapter>>any(), Matchers.<Set<String>>any());
        assertThat(cacheRefresh.isRunning(), is(false));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldCallRefreshFromTimer() throws Exception {
        // given
        when(requestMaker.isConnectionAvailable()).thenReturn(true);
        when(switcher.isRefreshCycleSwitchedOn()).thenReturn(true);

        // when
        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 1L);
        Thread.sleep(1200);

        // then
        verify(requestMaker, times(1)).makeRequests(Matchers.<Set<HttpRequestAdapter>>any(), Matchers.<Set<String>>any());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldNotCallRefreshFromTimerWhenFeatureDisabled() throws Exception {
        // given
        when(requestMaker.isConnectionAvailable()).thenReturn(true);
        when(switcher.isRefreshCycleSwitchedOn()).thenReturn(false);

        // when
        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 1L);
        Thread.sleep(1200);

        // then
        verify(requestMaker, never()).makeRequests(Matchers.<Set<HttpRequestAdapter>>any(), Matchers.<Set<String>>any());
    }

    @Test
    public void shouldDeinstallTimerWhenAsked() throws Exception {
        // given
        when(requestMaker.isConnectionAvailable()).thenReturn(true);
        when(switcher.isRefreshCycleSwitchedOn()).thenReturn(true);

        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 1L);

        // when
        cacheRefresh.cancelTimer();
        Thread.sleep(1200);

        // then
        verify(requestMaker, never()).makeRequests(Matchers.<Set<HttpRequestAdapter>>any(), Matchers.<Set<String>>any());
    }

    @Test
    public void shouldReinstallTimerWhenAsked() throws Exception {
        // given
        when(requestMaker.isConnectionAvailable()).thenReturn(true);
        when(switcher.isRefreshCycleSwitchedOn()).thenReturn(true);

        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 1L);

        // when
        cacheRefresh.cancelTimer();
        cacheRefresh.reinstallTimer();

        Thread.sleep(2000);

        // then
        verify(requestMaker, times(1)).makeRequests(Matchers.<Set<HttpRequestAdapter>>any(), Matchers.<Set<String>>any());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void clearAllExceptRefreshCycleCachesShouldDoWhatItSays() {
        // given
        cacheManager = mock(CacheManager.class);
        Ehcache cache1 = mock(Ehcache.class);
        Ehcache cache2 = mock(RequestMemoizingCache.class);
        when(cacheManager.getCacheNames()).thenReturn(new String[]{"cache1", "cache2"});
        when(cacheManager.getEhcache("cache1")).thenReturn(cache1);
        when(cacheManager.getEhcache("cache2")).thenReturn(cache2);

        cacheRefresh = new RefreshCycleManager(cacheManager, requestMaker, switcher, 0L);

        // when
        cacheRefresh.clearAllExceptRefreshCycleCaches();

        // then
        verify(cache1, times(1)).removeAll();
        verify(cache2, never()).removeAll();
    }

    private Ehcache getACache() {
        Ehcache underlyingCache = new Cache("acache", 1000, true, true, 1000L, 1000L);
        RequestMemoizingCache cache = new RequestMemoizingCache(
                underlyingCache,
                Arrays.asList((KeyBuilder)new UrlKeyBuilder()));
        return cache;
    }
}
