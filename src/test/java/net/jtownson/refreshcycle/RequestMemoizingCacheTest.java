package net.jtownson.refreshcycle;

import net.jtownson.refreshcycle.keybuilders.KeyBuilder;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.log4j.MDC;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class RequestMemoizingCacheTest {

    private CacheManager cacheManager;
    private TestKeyBuilder keyBuilder;
    private RequestMemoizingCache cache;

    @Before
    public void setup() throws Exception {
        keyBuilder = new TestKeyBuilder();

        cacheManager = CacheManager.create(new ClassPathResource("ehcache-failsafe.xml").getURL());

        cacheManager.removalAll();
        Ehcache underlyingCache = new Cache("acache", 1000, true, true, 1000L, 1000L);
        cache = new RequestMemoizingCache(
                underlyingCache,
                Arrays.asList((KeyBuilder) keyBuilder));
        cacheManager.addCache(cache);

    }

    @After
    public void teardown() {
        cacheManager.shutdown();
        endRequest();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldMemoizeOnlyOneRequestWhenTwoFragmentsMemoizeTwoEquivalentRequests() {

        // given
        // when
        makeRequest(new MockHttpServletRequest("GET", "/index.html"));
        cache.put(new Element("f1", "<div>a fragment</div>"));

        makeRequest(new MockHttpServletRequest("GET", "/index.html"));
        cache.put(new Element("f2", "<div>another fragment</div>"));

        // then
        assertThat(cache.getMemoizedRequestSet().size(), is(1));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldMemoizeBothRequestsWhenTwoFragmentsMemoizeNonEquivalentRequests() {

        // given

        // when
        makeRequest(new MockHttpServletRequest("GET", "/index.html"));
        cache.put(new Element("f1", "<div>a fragment</div>"));

        makeRequest(new MockHttpServletRequest("GET", "/browse.html"));
        cache.put(new Element("f2", "<div>another fragment</div>"));

        // then
        assertThat(cache.getMemoizedRequestSet().size(), is(2));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldMemoizeOnlyOneRequestWhenOneFragmentMemoizesTwoNonEquivalentRequests() {

        // given

        // when
        makeRequest(new MockHttpServletRequest("GET", "/index.html"));
        cache.put(new Element("f1", "<div>a fragment</div>"));

        makeRequest(new MockHttpServletRequest("GET", "/browse.html"));
        cache.put(new Element("f1", "<div>a fragment</div>"));

        // then
        assertThat(cache.getMemoizedRequestSet().size(), is(1));
    }

    @Test
    public void getShouldStillWorkWithoutPut() {

        // given
        String key = "key";

        // when

        // then
        assertThat(cache.get(key), Matchers.<Object>nullValue());
    }

    @Test
    public void putThenGetShouldStillWorkProperly() {

        // given
        String key = "key";
        String val = "val";

        // when
        cache.put(new Element(key, val));

        // then
        assertThat(cache.get(key), notNullValue());
        assertThat(cache.get(key).getValue(), instanceOf(String.class));
    }

    @Test
    public void putThenGetShouldReturnNullForRefreshRequest() {

        // given
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/index.html");
        request.addHeader("x-jmt-refresh-cache-region", "acache, another_cache_not_me");

        // when
        makeRequest(request);
        cache.put(new Element("f1", "<div>a fragment</div>"));

        // then
        assertThat(cache.get("f1"), Matchers.<Object>nullValue());
    }

    @Test
    public void putThenGetShouldWorkIfOtherCacheNameIsRefreshingButNotThisOne() {

        // given
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/index.html");
        request.addHeader("x-jmt-refresh-cache-region", "another_cache_not_me");

        // when
        makeRequest(request);
        cache.put(new Element("f1", "<div>a fragment</div>"));

        // then
        assertThat(cache.get("f1"), notNullValue());
    }

    @Test
    public void shouldRemoveNonRefreshedKeysAfterRefreshCycle() {

        // given (somebody makes a couple of requests and the fragmentTool does some caching)
        makeRequest(new MockHttpServletRequest("GET", "/index.html"));
        cache.put(new Element("f1", "<div>a fragment</div>"));

        makeRequest(new MockHttpServletRequest("GET", "/index.html"));
        cache.put(new Element("f2", "<div>another fragment</div>"));

        // when (refresh cycle runs and only one key is refreshed)
        cache.setIsInRefreshCycle(true);

        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/index.html");
        request.addHeader(RefreshConstants.REFRESH_CACHE_REGION_HEADER, cache.getName());

        makeRequest(request);
        cache.put(new Element("f1", "<div>an updated fragment</div>"));
        endRequest();

        cache.setIsInRefreshCycle(false);

        // then (the stale, non-refreshed f2 value is gone but the f1 value is the new value)
        assertThat(cache.get("f1"), notNullValue());
        assertThat(cache.get("f2"), nullValue());
        assertThat((String)cache.get("f1").getValue(), is("<div>an updated fragment</div>"));
    }

    @Test
    public void shouldOnlyRemoveKeysAfterRefreshCycleThatArePartOfTheRequestSubset() {

        // given (somebody makes a couple of requests and the fragmentTool does some caching)
        HttpServletRequest r1 = new MockHttpServletRequest("GET", "/index.html");
        makeRequest(r1);
        cache.put(new Element("f1", "<div>a fragment</div>"));

        makeRequest(new MockHttpServletRequest("GET", "/index.html"));
        cache.put(new Element("f2", "<div>another fragment</div>"));

        Set<HttpRequestAdapter> requestSubset = new HashSet<HttpRequestAdapter>();

        // when
        // (refresh cycle runs but keys are not refreshed
        // but that was the *correct* thing to do because this was
        // a conditional request)
        cache.setIsInRefreshCycle(true, requestSubset);

        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/index.html");
        request.addHeader(RefreshConstants.REFRESH_CACHE_REGION_HEADER, cache.getName());

        makeRequest(request);
        // no cache puts during the request
        endRequest();

        cache.setIsInRefreshCycle(false);

        // then we should still have our keys
        assertThat(cache.get("f1"), notNullValue());
        assertThat(cache.get("f2"), notNullValue());
        assertThat((String)cache.get("f1").getValue(), is("<div>a fragment</div>"));
        assertThat((String)cache.get("f2").getValue(), is("<div>another fragment</div>"));
    }

    private static void makeRequest(HttpServletRequest request) {
        MDC.put(RefreshCycleFilter.HTTP_REQUEST, request);
    }

    private static void endRequest() {
        MDC.remove(RefreshCycleFilter.HTTP_REQUEST);
    }

    static class TestKeyBuilder implements KeyBuilder {

        public void appendToKey(HttpServletRequest request, StringBuilder stringBuilder) {
            stringBuilder.append(request.getMethod()).append(request.getRequestURI());
        }

        public void appendToRequest(HttpServletRequest request, HttpRequestAdapter requestAdapter) {

        }
    }

}
